package it.vali;

public class Main {

    public static void main(String[] args) {
        String sentence = "Kevadel elutses metsas mutionu keset Kuuski noori vanu.";
//      leia üles esimene tühik. Mis on tema indeks?
        int spaceIndex = sentence.indexOf(" ");
//        indexOf tagastab -1, kui otsitavat sümbolit ei leitud
//        indexOf tagastab indeksi, kust sümbol (või fraas leitakse)
//        spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
//        System.out.println(spaceIndex);

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        }
        System.out.println();

        spaceIndex = sentence.lastIndexOf(" ");
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);
        }

        System.out.println("Prindi lause esimene sõna:");
        spaceIndex = sentence.indexOf(" ");
        String part = sentence.substring(0, spaceIndex);
        System.out.println(part);

        System.out.println("Prindi lause teine sõna:");
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        part = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(part);

        System.out.println("Leia esimene k-tähega algav sõna:");

        // Leia esimene k-tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna

        String firstLetter = sentence.substring(0, 1);

        String kWord = "";

        if (firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0, spaceIndex);
        } else {
            int kIndex = sentence.indexOf(" k") + 1;
            if (kIndex != 0) {
                spaceIndex = sentence.indexOf(" ", kIndex);

                kWord = sentence.substring(kIndex, spaceIndex);
            }
        }
        if (kWord.equals("")) {
            System.out.println("Puudub k-tähega algav sõna");
        } else {
            System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
        }


        System.out.println("Leia, mitu sõna sul lauses on");
        int spaceCounter = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }
        System.out.printf("Sõnade arv lauses on %d.%n", spaceCounter + 1);

        System.out.println("Leia mitu k-tähega algavat sõna lauses on.");
        int kCounter = 0;
        sentence = sentence.toLowerCase();

        int kIndex = sentence.indexOf(" k");
        while (kIndex != -1) {
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kCounter++;
        }

        if (sentence.substring(0, 1).equals("k")) {
            kCounter++;
        }
        System.out.printf("K tähega algavate sõnade arv lauses on %d%n.", kCounter);

    }
}
