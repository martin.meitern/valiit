package it.vali;

public class Main {

    public static void main(String[] args) {
//	      Method overriding ehk meetodi ülekirjutamine
//        Päritava klassi meetodi meetodi sisu kirjutatakse pärinevas klassis üle.

        Dog buldog = new Dog();
        Cat siam = new Cat();

        siam.eat();
        buldog.eat();
        siam.setLivesOutside(false);
      

        siam.printInfo();

//        Kirjuta koera getAge üle nii, et kui koera vanus on 0, siis näitaks ühte

        System.out.println(buldog.getAge());

        buldog.printInfo();
//        Metsloomadel printinfo võiks kirjutada "Nimi: metsloomal pole nime
        CarnivoreAnimal lion = new CarnivoreAnimal();
        lion.setName("Simba");
        lion.printInfo();
    }
}