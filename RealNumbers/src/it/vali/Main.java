package it.vali;

public class Main {

    public static void main(String[] args) {
        float a = 123.34f;
        double b = 123.4343;

        System.out.printf("Arvude %e ja %e summa on %e%n", a, b, a + b);
        System.out.printf("Arvude %f ja %f summa on %f%n", a, b, a + b);
        System.out.printf("Arvude %g ja %g summa on %g%n", a, b, a + b);

        /* float ja double numbrite jagamisel tuleb välja komakohtade täpsust
         */
        float c = 130000f;
        float e = 23000000f;
        double f = 130000;
        double g = 23000000;
        System.out.println(c / e);
        System.out.println(f / g);

        System.out.println(c * e);
        System.out.println(f * g);

    }
}
