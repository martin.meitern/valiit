package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
//	    int[] numbers = new int[5];
//
//        for (int i = 0; i < numbers.length; i++) {
//            System.out.println(numbers[i]);
//        }
//
////        lisa massiivi 5 numbrit
//
//        numbers[0] = 1;
//        numbers[1] = 2;
//        numbers[2] = 3;
//        numbers[3] = 4;
//        numbers[4] = 5;
//
////        eemalda teine number
//
//        numbers[1] = 0;
//
//        for (int i = 0; i < numbers.length; i++) {
//            System.out.println(numbers[i]);
//        }
//
//        int[] numbersNew = new int[] {numbers[0], numbers[3], numbers[2], numbers[1], numbers[4]};
//
//        for (int i = 0; i < numbersNew.length; i++) {
//            System.out.println(numbersNew[i]);
//        }

//        tavalisse arraylisti võib lisada ükskõik, mis tüüpi elemente.
//        Aga elemente küsides sealt peab teadma, mis tüüpi element kus täpselt asub
//        ning pean siis selleks tüübiks küsimisel ka teisendama/cast'ima
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);

        int a = (int) list.get(1);
        String word = (String) list.get(0);

        String sentence = list.get(0) + " hommikust";
        System.out.println(sentence);
        System.out.println();
        if (Integer.class.isInstance(list.get(1))) {
            System.out.println("listis element indeksiga 1 on int");
        }
        System.out.println();

        list.add(2, "head aega");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(1);
        otherList.add(2.4343);

        list.addAll(otherList);

        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println();
        if (list.contains(money)) {
            System.out.println("Money asub listis");
        }
        System.out.println();

        System.out.printf("Money asub listis indeksiga %d%n", list.indexOf(money));
        System.out.println();

        list.remove(money);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        list.remove(5);

    }
}
