package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEE");
        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DATE, -1);
        Date date = calendar.getTime();
//        System.out.println(dateFormat.format(date));
//        calendar.add(Calendar.YEAR, 1);
        date = calendar.getTime();
//        System.out.println(dateFormat.format(date));

//        Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäev

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 01);
        date = calendar.getTime();
        DateFormat weekdayFormat = new SimpleDateFormat("dd.MM.yyyy EEEE");

        int monthsLeftThisYear = 11 - calendar.get(Calendar.MONTH);
//        for (int i = 0; i < monthsLeftThisYear; i++) {
//            calendar.add(Calendar.DATE, 1);
//            date = calendar.getTime();
//            System.out.println(weekdayFormat.format(date));
//        }
        int currentYear = calendar.get(Calendar.YEAR);

        calendar.add(Calendar.MONTH, 1);

        while (calendar.get(Calendar.YEAR) == currentYear) {
            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);
        }
    }
}
