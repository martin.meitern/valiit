    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Qualifications List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Employee ID</th><th>Qualifications</th><th>Level of qualification</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="qualifications" items="${list}"> 
    <tr>
    <td>${qualifications.id}</td>
    <td>${qualifications.employee_id}</td>
    <td>${qualifications.employeeQualifications}</td>
    <td>${qualifications.levels}</td>
    <td><a href="editqualifications/${qualifications.id}">Edit</a></td>
    <td><a href="deletequalifications/${qualifications.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="qualificationsform">Add New Qualification</a>
