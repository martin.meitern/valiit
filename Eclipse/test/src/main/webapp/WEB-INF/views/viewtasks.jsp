    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

	<h1>Tasks List |Signed in as ${pageContext.request.userPrincipal.name}| <td><a href="../employee/viewemployee">Home</a></td> | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h1>    
	
	   </c:if>
	   
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Description</th><th>Is the task done</th><th>Due date</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="tasks" items="${list}"> 
    <tr>
    <td>${tasks.id}</td>
    <td>${tasks.description}</td>
    <td>${tasks.done}</td>
     <td>${tasks.duedate}</td>
    <td><a href="edittasks/${tasks.id}">Edit</a></td>
    <td><a href="deletetasks/${tasks.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="tasksform">Add New Task</a>

    

 

</div>