<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Employee</h1>
       <form:form method="post" action="save?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data" >  
      	<table >  
         <tr>  
          <td>First name : </td> 
          <td><form:input path="firstName"  /></td>
         </tr>  
         <tr>  
          <td>Last name : </td> 
          <td><form:input path="lastName"  /></td>
         </tr>
         <tr>  
          <td>Personal ID : </td> 
          <td><form:input path="personalId"  /></td>
         </tr>
         <tr>  
          <td>Designation :</td>  
          <td><form:input path="designation" /></td>
         </tr> 
   		
         <tr>  
          <td>Working status :</td> 
           <td width=55%>
	          <select name="archived" style="width:152px">
	            <option value="0">Working</option>
				<option value="1">Archived</option>			 
			  </select>	  		
			</td>   
         </tr>
          
          <tr>  
          <td>User Id :</td>  
          <td><form:select path="userId" items="${users}" /></td>
         </tr> 
         
         <tr>
            <td colspan="2">
            File to upload: <input type="file" name="file">
            </td>
        </tr>
   		 
   		 <tr>
         <td> </td>
         <td><br><input type="submit" value="Save" /></td>
         </tr>          
        </table>  
         
         
       </form:form>  
