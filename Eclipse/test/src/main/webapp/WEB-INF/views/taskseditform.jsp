<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Edit Task | <td><a href="../../employee/viewemployee">Home</a></td> | <a href="login?logout=true">Log out</a></h1>
       <form:form method="POST" action="/account/tasks/editsave"> 
      	<table >  
      	<tr>
      	<td></td>  
         <td><form:hidden  path="id" /></td>
         </tr> 
         <tr>  
          <td>Description : </td> 
          <td><form:input path="description"  /></td>
         </tr>  
         <tr>  
          <td>Is it done :</td>  
          <td width=55%>
	          <select name="archived" style="width:152px">
	            <option value="0">Not done</option>
				<option value="1">Done</option>			 
			  </select>						  		
			</td> 
			</tr> 
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Edit Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
