    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover {
  background-color: #111;
}
</style>
</head>
<body>

<ul>
  <li><a class="active" href="#home">Home</a></li>
  <li><a href="#news">News</a></li>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#about">About</a></li>
</ul>

</body>
</html>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    
    
    
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bootstrap CRUD Data Table for Database with Modal Form</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">

	
  body {
        color: #566787;
		background: #f5f5f5;
		font-family: 'Roboto Expanded', sans-serif;
		font-size: 14px;
		
	}

	</style>
	<script type="text/javascript">
	$(document).ready(function(){
		// Activate tooltip
		$('[data-toggle="tooltip"]').tooltip();
		
		// Select/Deselect checkboxes
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function(){
			if(this.checked){
				checkbox.each(function(){
					this.checked = true;                        
				});
			} else{
				checkbox.each(function(){
					this.checked = false;                        
				});
			} 
		});
		checkbox.click(function(){
			if(!this.checked){
				$("#selectAll").prop("checked", false);
			}
		});
	});
	</script>
	    
    
    
    
    
</head>
<body>
<div class="container">

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

        <h2>Welcome ${pageContext.request.userPrincipal.name} | <td><a href="../tasks/viewtasks">View tasks</a></td> | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
       

    </c:if>
    

</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

    



    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Employees <b>List</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Employee</span></a>
						<a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						
					</div>
                </div>
            </div>
            
            
            <form class="example" action="/account/employee/search">
			  <input type="text" class="form-control" placeholder="Search.." name="search2" >
			  	  	  
			  <select name="orderby" class="form-control-two">
			    <option value="id" selected>Sort by:</option>
			  	<option value="firstName">First Name</option>
			    <option value="lastName">Last Name</option>
				<option value="date">Adding Time</option>	
				<option value="employeeQualifications">Qualification</option>		 
			  </select>
		           
			  <select name="count" class="form-control-two">
			   <option value="100" selected>Show:</option>
			   <option value="10">10</option>
			   <option value="20">20</option>
			   <option value="100">All</option>			 
		      </select>
		    	  
		      <button type="submit" class="btn btn-dark" >Submit</button>	  
			  
			</form>
            
            
            <table class="table table-striped table-hover">
                                  
				<tr><br><th>Id</th><th>First name</th><th>Last name</th><th>Personal ID</th><th>Designation</th>
			
				<th>Qualification</th><th>Level</th><c:if test="${isAdmin}"><th>Archived</th></c:if><th>Adding Time</th><th>Picture</th><th>Actions</th><c:if test="${isAdmin}"><th>Delete</th></c:if></tr>
			
			    <c:forEach var="employee" items="${list}"> 
			    <tr>
				    <td>${employee.id}</td>
				    <td>${employee.firstName}</td>
				    <td>${employee.lastName}</td>
				    <td>${employee.personalId}</td>
				    <td>${employee.designation}</td>
				    <td>${employee.qualification}</td>
				    <td>${employee.level}</td>
				    <c:if test="${isAdmin}"><td>${employee.archived}</td></c:if>
				    <td>${employee.date}</td>
				    
				    
				    <td><a href="getPhoto/${employee.id}">Photo</a></td>
				    
				    <c:if test="${isAdmin}"><td><a href="deleteemployee/${employee.id}">Delete</a></td></c:if>
			    	
			    	<td>
			    	   <a href="employeeview/${employee.id}" class="view"><i class="material-icons" data-toggle="tooltip" title="View">&#xE417;</i></a>
			    	   <a href="editemployee/${employee.id}" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                       <c:if test="${isAdmin}"><a href="deleteemployee/${employee.id}" class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a></c:if>
                             
                    </td>
			    
			    </tr>
			    </c:forEach>
			    		    			    
            </table>
            
            
            	
    <br/>
    <c:if test="${isAdmin}"><a href="employeeform">Add New Employee  | </a></c:if>

	<a href="../qualification/qualificationsform">Add Qualifications</a>
            
            
            
		
        </div>
    </div>
	
</body>
</html>





                                		                            