<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Qualification</h1>
       <form:form method="post" action="save">  
      	<table >
           

           <tr>
     	<td></td>  
        <td><form:hidden  path="employee_id" /></td>
        </tr>  
           
           
			<tr>  
          <td>Qualification : </td> 
   		 <td width=55%> 
          <form:select style="width:152px" path="employeeQualifications">
		  	<form:option value="1" label="Java"/>
		  	<form:option value="2" label="Python"/>
		  	<form:option value="3" label="C"/>
		  	<form:option value="4" label="C++"/>
		  	<form:option value="5" label="C#"/>
		  	<form:option value="6" label="R"/>
		  	<form:option value="7" label="JavaScript"/>
		  	<form:option value="8" label="GO"/>
		  	<form:option value="9" label="Swift"/>
		  	<form:option value="10" label="Ruby"/>
		  	<form:option value="11" label="PHP"/>
		  	<form:option value="12" label="SQL"/>
		  	<form:option value="13" label="HTML"/>
		  	<form:option value="14" label="CSS"/>
		  			
		   </form:select>
		   </td>		 
		</tr>
		
         <tr>  
          <td>Experience level :</td> 
			<td width=55%> 
          <form:select style="width:152px" path="levels">
		  	<form:option value="1" label="N/A"/>
		  	<form:option value="2" label="Beginner"/>
		  	<form:option value="3" label="Intermediate"/>
		  	<form:option value="4" label="Advanced"/>
		  	<form:option value="5" label="Expert"/>
		   </form:select>
		   </td>		 
		</tr>
         
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
