<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


	<h1>Edit Employee</h1>
      <form:form method="POST" action="/account/employee/editsave">  
     	<table >  
     	<tr>
     	<td></td>  
        <td><form:hidden  path="id" /></td>
        </tr> 
        <tr>  
         <td>First name : </td> 
         <td><form:input path="firstName"  /></td>
        </tr>  
        <tr>  
         <td>Last name : </td> 
         <td><form:input path="lastName"  /></td>
        </tr>
      
        <tr>  
         <td>Designation :</td>  
         <td><form:input path="designation" /></td>
        </tr> 
        <tr>  
         <td>Date :</td>  
         <td><form:input path="date" /></td>
        </tr>
        
        <tr>  
         <td>Working status :</td> 
          <td width=55%>
          <select name="archived" style="width:152px">
            <option value="0">Working</option>
			<option value="1">Archived</option>			 
		  </select>						  		
		</td>   
        </tr>
        
        <tr>  
          <td>User Id :</td>  
          <td><form:select path="userId" items="${users}" /></td>
         </tr> 
	
        <tr>
         <td> </td> 
	  <td><a href="../../employee/qualificationsform/${command.id}">Add Qualifications</a></td>
	</tr>
	
  		 <tr>
         <td> </td>
         <td><br><input type="submit" value="Save" /></td>
        </tr>  
       </table>     
       
         <table border="2" width="40%" cellpadding="2">
		   <tr><br><th>Qualification</th><th>Level</th><th>Edit</th><th>Delete</th></tr>
		    <c:forEach var="qualification" items="${qualifications}"> 
		    <tr>		    
		    <td>${qualification.qualificationName}</td>
		    <td>${qualification.levelName}</td>
		    <td><a href="../editqualification/${qualification.id}">Edit</a></td>
		    <td><a href="../deletequalification/${qualification.id}">Delete</a></td>		    
		    </tr>
		  </c:forEach>
		</table>
       
        
      
      </form:form>  
      
      
      
	       


