<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


	<h1>${employee.firstName} ${employee.lastName}</h1>
    <table border="2" width="100%" cellpadding="2">
	<table border="2" width="100%" cellpadding="2">
	<tr><br><th>First name</th><th>Last name</th><th>Personal ID</th><th>Designation</th>
	<th>Archived</th><th>Adding Time</th></tr>
    <tr>
    <td>${employee.firstName}</td>
    <td>${employee.lastName}</td>
    <td>${employee.personalId}</td>
    <td>${employee.designation}</td>
    <td>${employee.archived}</td>
    <td>${employee.date}</td>
    </tr>
    </table>
    
    <h1>Trainings and Exams</h1>
	<table border="2" width="50%" cellpadding="2">
	<tr><th>Trainings and Exams</th><th>Certificate</th></tr>
    <tr>
    <td>${trainings.trainings_and_exams}</td>
     <td><a href="getPhoto/${trainings.id}">Certificate</a></td>
    </tr>
    </table>
    
    <table border="2" width="20%" cellpadding="2">
		   <tr><br><th>Qualification</th><th>Level</th></tr>
		    <c:forEach var="qualification" items="${qualifications}"> 
		    <tr>		    
		    <td>${qualification.qualificationName}</td>
		    <td>${qualification.levelName}</td> 
		    </tr>
		  </c:forEach>
		</table>
    			
    
    <td><a href="editemployee/${employee.id}">Edit</a></td>
    <td><a href="deleteemployee/${employee.id}">Delete</a></td>