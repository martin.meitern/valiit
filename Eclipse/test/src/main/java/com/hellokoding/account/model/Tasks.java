package com.hellokoding.account.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Tasks {  
	private int id;  
	private String description;
	private boolean done;
	private Date duedate;
	private int TaskOwnerID;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
	
	  
	public int getId() {  
	    return id;  
	}  
	public void setId(int id) {  
	    this.id = id;  
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public int getTaskOwnerID() {
		return TaskOwnerID;
	}
	public void setTaskOwnerID(int taskOwnerID) {
		TaskOwnerID = taskOwnerID;
	}
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public Date getDuedate() {
		return duedate;
	}
	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

  
}  