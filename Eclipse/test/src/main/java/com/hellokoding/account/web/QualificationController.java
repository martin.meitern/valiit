package com.hellokoding.account.web;   
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;   
import com.hellokoding.account.model.Qualifications;  
import com.hellokoding.account.repository.QualificationDao;  
@Controller 
@RequestMapping("qualification")
public class QualificationController {  
    @Autowired  
    QualificationDao dao; 
      
    @RequestMapping("/qualificationsform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Qualifications());
    	return "qualificationsform"; 
    }  
 
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("qualifications") Qualifications qualifications){  
        dao.save(qualifications);  
        return "redirect:/qualification/viewqualifications"; 
    }  

    @RequestMapping("/viewqualifications")  
    public String viewqualifications(Model m){  
        List<Qualifications> list=dao.getQualifications();  
        m.addAttribute("list",list);
        return "viewqualifications";  
    }  
  
    @RequestMapping(value="/editqualifications/")  
    public String edit(@PathVariable int id, Model m){  
        Qualifications qualifications=dao.getQualificationById(id);  
        m.addAttribute("command",qualifications);
        return "qualificationseditform";  
    }  
 
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("qualifications") Qualifications qualifications){  
        dao.update(qualifications);  
        return "redirect:viewqualifications";  
    }  
 
    @RequestMapping(value="/deletequalifications/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/qualification/viewqualifications";  
    }   
}  