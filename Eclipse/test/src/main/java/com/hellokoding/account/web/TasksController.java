package com.hellokoding.account.web;   
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;

import com.hellokoding.account.model.Urgency;
import com.hellokoding.account.model.Tasks;
import com.hellokoding.account.repository.TasksDao;  
@Controller 
@RequestMapping("tasks")
public class TasksController {  
    @Autowired  
    TasksDao dao;//will inject dao from xml file  
      
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
    @RequestMapping("/tasksform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Tasks());
    	
    	m.addAttribute("TaskOwner", TaskOwnerToMap(dao.getTasks()));
    	
    	return "tasksform"; 
    }  
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("tasks") Tasks tasks){  
        dao.save(tasks);  
        return "redirect:viewtasks";//will redirect to viewemp request mapping  
    }  
    /* It provides list of employees in model object */  
    @RequestMapping("/viewtasks")  
    public String viewemp(Model m){  
        List<Tasks> list=dao.getEmployees();  
        m.addAttribute("list",list);
        return "viewtasks";  
    }  
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.*/  
    @RequestMapping(value="/edittasks/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Tasks tasks=dao.getEmpById(id);  
        m.addAttribute("command",tasks);
        m.addAttribute("TaskOwner", TaskOwnerToMap(dao.getTasks()));
        return "taskseditform";  
    }  
    /* It updates model object. */  
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("tasks") Tasks tasks){  
        dao.update(tasks);  
        return "redirect:viewtasks";  
    }  
    /* It deletes record for the given id in URL and redirects to /viewemp */  
    @RequestMapping(value="/deletetasks/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:../viewtasks";  
    }   
    
    private Map<Integer, String> TaskOwnerToMap(List<Urgency> taskOwners) {
    	Map<Integer, String> taskOwnerMap = new HashMap<>();
    	
    	for(Urgency taskowner : taskOwners) {
    		taskOwnerMap.put(taskowner.getId(), taskowner.getName());
    	}
    	
    	return taskOwnerMap;
    }
}  