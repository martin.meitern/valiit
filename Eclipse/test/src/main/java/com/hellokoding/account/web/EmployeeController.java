package com.hellokoding.account.web; 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.hellokoding.account.service.UserService; 

import com.hellokoding.account.model.Employee;

import com.hellokoding.account.model.EmployeeQualification;

import com.hellokoding.account.model.Qualifications;
import com.hellokoding.account.model.User;

import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.QualificationDao;
import com.hellokoding.account.repository.UserRepository;

@Controller
@RequestMapping("employee")
public class EmployeeController {  
    
	@Autowired  
    EmployeeDao dao;
	
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    
    

    
    @RequestMapping("/employeeform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Employee());
    	m.addAttribute("users", usersToMap(userRepository.findAll()));
    	return "employeeform"; 
    }  
    
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("employee") Employee employee, @RequestParam("file") MultipartFile file){
    	try {
            employee.setPicture(file.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        dao.save(employee);  
        return "redirect:/employee/viewemployee";
    }  
    
    @RequestMapping(value = "/getPhoto/{id}")
    public void getPhoto(HttpServletResponse response, @PathVariable("id") int id) throws Exception {
        response.setContentType("image/jpeg");
        
        Employee employee = dao.getEmployeeById(id);
        byte[] bytes = employee.getPicture();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        IOUtils.copy(inputStream, response.getOutputStream());
    } 
    
    
   
    @RequestMapping("/viewemployee")  
    public String viewemployee(Model m){  
    	
    	
    	
    	
        List<Employee> list=dao.getEmployees();  
        m.addAttribute("list",list);
        boolean isAdmin =  userService.hasRole("ROLE_ADMIN");
        m.addAttribute("isAdmin", userService.hasRole("ROLE_ADMIN")); 
        return "viewemployee";  
    }  
    
    @RequestMapping("/employeeview/{id}")  
    public String viewOneEmployee(@PathVariable int id, Model m){  
    	Employee employee=dao.getEmployeeById(id);  
    	m.addAttribute("employee", dao.getEmployeeById(id));
        m.addAttribute("qualifications", dao.getQualifications(id));
        return "employeeview";  
    }
    
    @RequestMapping(value="/editemployee/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Employee employee=dao.getEmployeeById(id);  
        m.addAttribute("command",employee);
        m.addAttribute("qualifications", dao.getQualifications(id));
        m.addAttribute("users", usersToMap(userRepository.findAll()));
        return "employeeeditform";  
    }  
    
    @RequestMapping(value="/editqualification/{id}")  
    public String editQualification(@PathVariable int id, Model m){  

        m.addAttribute("command",dao.getQualificationById(id));
        
        return "editqualification";  
    }  
   
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("employee") Employee employee){  
        dao.update(employee);  
        return "redirect:/employee/viewemployee";  
    }  
      
    @RequestMapping(value="/deleteemployee/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/employee/viewemployee";  
    }   
    
    @RequestMapping(value="/deletequalification/{id}",method = RequestMethod.GET)  
    public String deleteQualification(@PathVariable int id){ 
    	int employeeId = dao.getQualificationById(id).getEmployeeId();
        dao.deleteQualification(id);  
        return "redirect:/employee/editemployee/" + employeeId;  
    }   
    
    @RequestMapping(value="/search",method = RequestMethod.GET)  
    public String search(Model m, int count, String orderby, String search2){      
        List<Employee> list=dao.search(count, orderby, search2);  
        m.addAttribute("list",list);
        return "viewemployee";  
    } 
    
    @Autowired  
    QualificationDao daoQualification;  
    
    
   @RequestMapping("/qualificationsform/{id}")  
    public String showqualificationform(@PathVariable int id, Model m){  
    	m.addAttribute("qualifications", dao.getQualifications(id));
    	Qualifications qualifications = new Qualifications();
    	qualifications.setEmployee_id(id);
    	
    	m.addAttribute("command", qualifications);
    	return "qualificationsform"; 
    } 
    
   
    @RequestMapping(value="/qualificationsform/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("qualifications") Qualifications qualifications){  
        daoQualification.save(qualifications);  
        return "redirect:../viewemployee"; 
    }  
    
    /*@RequestMapping(value="/qualificationsform/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("qualifications") Qualifications qualifications){ 
    	int employeeId = dao.getQualificationById(id).getEmployeeId();
        daoQualification.save(qualifications);  
        return "redirect:/employee/editemployee/" + employeeId;  
    }  */
    
    private Map<Long, String> usersToMap(List<User> users) {
    	Map<Long, String> usersMap = new HashMap<>();
    	
    	for(User user : users) {
    		usersMap.put(user.getId(),user.getUsername());
    	}
    	
    	return usersMap;
    }
}  