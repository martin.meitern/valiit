package com.hellokoding.account.repository;
import java.io.ByteArrayInputStream;
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.EmployeeQualification;
import com.hellokoding.account.model.Qualifications;

 
  
public class EmployeeDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
/*public int save(Employee p){  
    String sql="insert into Employee(firstName,lastName,personalId,designation,archived,date) "
    		+ "values('"+p.getFirstName()+"','"+p.getLastName()+"','"+p.getPersonalId()+"','"+p.getDesignation()+"',"+p.getArchived()+",'"+dateTime()+"' "+ ")";  
    return template.update(sql);  
}  */

public int save(Employee p){
    MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
    mapSqlParameterSource.addValue("firstName", p.getFirstName());
    mapSqlParameterSource.addValue("lastName", p.getLastName());
    mapSqlParameterSource.addValue("personalId", p.getPersonalId()); 
    mapSqlParameterSource.addValue("designation", p.getDesignation());
    mapSqlParameterSource.addValue("archived", p.getArchived());
    mapSqlParameterSource.addValue("userId", p.getUserId());
    mapSqlParameterSource.addValue("date", dateTime());
    mapSqlParameterSource.addValue("picture",  new SqlLobValue(new ByteArrayInputStream(p.getPicture()),
       p.getPicture().length, new DefaultLobHandler()), Types.BLOB);
       NamedParameterJdbcTemplate jdbcTemplateObject = new
                 NamedParameterJdbcTemplate(template.getDataSource());
    String sql = "insert into employee(firstName,lastName,personalId,designation,archived,userId,date,picture) "
            + "VALUES (:firstName,:lastName,:personalId,:designation,:archived,:userId,:date,:picture)";

   return jdbcTemplateObject.update(sql, mapSqlParameterSource);
}

public int update(Employee p){  
    String sql="update Employee set firstName='"+p.getFirstName()+"', lastName='"+p.getLastName()+"', designation='"+p.getDesignation()+"', archived="+p.getArchived()+", userId="+p.getUserId()+", date='"+p.getDate()+"'"+" where id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="delete from Employee where id="+id+"";  
    return template.update(sql);  
}  

public int deleteQualification(int id){  
    String sql="delete from employee_qualification where id="+id+"";  
    return template.update(sql);  
}  

public Employee getEmployeeById(int id){  
    String sql="select * from Employee where id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Employee>(Employee.class));  
}  

 public List<Employee> getEmployees(){  
    return template.query("SELECT \r\n" + 
    		"e.id,\r\n" + 
    		" e.firstName,\r\n" + 
    		" e.lastName,\r\n" + 
    		" e.personalId,\r\n" + 
    		"e.designation,\r\n" + 
    		"q.employeeQualifications,\r\n" + 
    		"eq.levelId,\r\n" + 
    		"e.archived,\r\n" + 
    		"e.date\r\n" + 
    		"FROM accounts.employee AS e\r\n" + 
    		"LEFT JOIN accounts.employee_qualification AS eq\r\n" + 
    		"ON e.id = eq.employeeId\r\n" + 
    		"LEFT JOIN accounts.qualifications_test AS q\r\n" + 
    		"ON eq.qualificationId = q.id",new RowMapper<Employee>(){  
        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
            Employee e=new Employee();  
            e.setId(rs.getInt(1));  
            e.setFirstName(rs.getString(2));  
            e.setLastName(rs.getString(3));   
            e.setPersonalId(rs.getString(4));  
            e.setDesignation(rs.getString(5)); 
            e.setQualification(rs.getString(6));
            e.setLevel(rs.getInt(7));
            e.setArchived(rs.getInt(8));
            e.setDate(rs.getString(9));
            return e;  
        }  
    });  
}  
 
 
 public List<EmployeeQualification> getQualifications(int employeeId){  
	    return template.query("SELECT eq.id, qualificationId, levelId, employeeQualifications AS qualificationName, levels AS levelName FROM qualifications_test AS qt "
	    		+ " RIGHT JOIN employee_qualification AS eq ON qt.id = eq.qualificationId"
	    		+ " JOIN levels AS l ON l.id = eq.levelId "
	    		+ " WHERE employeeId ="+employeeId,
	    		new RowMapper<EmployeeQualification>(){  
	        public EmployeeQualification mapRow(ResultSet rs, int row) throws SQLException {  
	        	EmployeeQualification employeeQualification = new EmployeeQualification();
	        	employeeQualification.setId(rs.getInt(1));
	        	employeeQualification.setEmployeeId(employeeId);
	        	employeeQualification.setQualificationId(rs.getInt(2));
	        	employeeQualification.setLevelId(rs.getInt(3));
	        	employeeQualification.setQualificationName(rs.getString(4));
	        	employeeQualification.setLevelName(rs.getString(5));
	        	return employeeQualification;
	        }  
	    });  
	}   
 



 public EmployeeQualification getQualificationById(int id){  
	    String sql="select * from employee_qualification where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<EmployeeQualification>(EmployeeQualification.class));  
	}  

private String dateTime() {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Calendar calendar = Calendar.getInstance();
	Date date = calendar.getTime();
	
	return dateFormat.format(date);
}



public List<Employee> search(int count, String orderby, String search2) {
	return template.query("SELECT \r\n" + 
			"e.id,\r\n" + 
			" e.firstName,\r\n" + 
			" e.lastName,\r\n" + 
			" e.personalId,\r\n" + 
			"e.designation,\r\n" + 
			"q.employeeQualifications,\r\n" + 
			"eq.levelId,\r\n" + 
			"e.archived,\r\n" + 
			"e.date\r\n" + 
			"FROM employee AS e\r\n" + 
			"LEFT JOIN employee_qualification AS eq\r\n" + 
			"ON e.id = eq.employeeId\r\n" + 
			"LEFT JOIN qualifications_test AS q\r\n" + 
			"ON eq.qualificationId = q.id where lower(firstName) like '%"
			+ search2.toLowerCase() +"%' OR lower(lastName) like '%"
			+ search2.toLowerCase() +"%' OR personalId like '%"
			+ search2 +"%' OR lower(employeeQualifications) like '%"
			+ search2.toLowerCase() +"%'ORDER BY " + orderby + " LIMIT " + count ,new RowMapper<Employee>(){
	        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
	            Employee e=new Employee();  
	            e.setId(rs.getInt(1));  
	            e.setFirstName(rs.getString(2));  
	            e.setLastName(rs.getString(3));   
	            e.setPersonalId(rs.getString(4));  
	            e.setDesignation(rs.getString(5)); 
	            e.setQualification(rs.getString(6));
	            e.setLevel(rs.getInt(7));
	            e.setArchived(rs.getInt(8));
	            e.setDate(rs.getString(9));
	            return e;  
	        }  
	    });  
	
}

}  