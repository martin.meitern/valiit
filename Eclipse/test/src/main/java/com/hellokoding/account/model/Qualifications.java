package com.hellokoding.account.model; 


public class Qualifications{

		private int id;  
		private int employee_id;
		private int employeeQualifications;  
		private int levels;  

		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		
		public int getEmployee_id() {
			return employee_id;
		}
		public void setEmployee_id(int employee_id) {
			this.employee_id = employee_id;
		}
		public int getEmployeeQualifications() {
			return employeeQualifications;
		}
		public void setEmployeeQualifications(int employeeQualifications) {
			this.employeeQualifications = employeeQualifications;
		}
		public int getLevels() {
			return levels;
		}
		public void setLevels(int levels) {
			this.levels = levels;
		}
}






