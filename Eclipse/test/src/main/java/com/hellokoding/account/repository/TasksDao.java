package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Urgency;
import com.hellokoding.account.model.Tasks;  
  
public class TasksDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
public int save(Tasks p){  
    String sql="INSERT INTO Tasks(description,done,duedate,TaskOwnerID) values('"+p.getDescription()+"',"+p.isDone()+","+p.getDuedate()+","+p.getTaskOwnerID()+")";  
    return template.update(sql);  
}  
public int update(Tasks p){  
    String sql="UPDATE Tasks SET description='"+p.getDescription()+"', done="+p.isDone()+", duedate="+p.getDuedate()+", TaskOwnerID="+p.getTaskOwnerID()+" WHERE id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="DELETE FROM Tasks WHERE id="+id+"";  
    return template.update(sql);  
}  
public Tasks getEmpById(int id){  
    String sql="SELECT * FROM Tasks WHERE id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Tasks>(Tasks.class));  
}  
public List<Tasks> getEmployees(){  
    return template.query("SELECT * FROM Tasks",new RowMapper<Tasks>(){  
        public Tasks mapRow(ResultSet rs, int row) throws SQLException {  
        	Tasks e=new Tasks();  
            e.setId(rs.getInt(1));  
            e.setDescription(rs.getString(2));  
            e.setDone(rs.getBoolean(3));
            e.setDuedate(rs.getDate(4));
            return e;  
        }  
    });  
}  
public List<Urgency> getTasks(){  
    return template.query("SELECT id,name FROM taskowner",new RowMapper<Urgency>(){  
        public Urgency mapRow(ResultSet rs, int row) throws SQLException {  
        	Urgency urgency=new Urgency();  
            urgency.setId(rs.getInt(1));  
            urgency.setName(rs.getString(2));  
            return urgency;  
        }  
    });  
}  
}  