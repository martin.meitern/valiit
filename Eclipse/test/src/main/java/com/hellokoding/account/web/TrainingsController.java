package com.hellokoding.account.web; 
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.hellokoding.account.model.Trainings;
import com.hellokoding.account.repository.TrainingsDao; 

@Controller  
@RequestMapping("trainings")
public class TrainingsController {  
    @Autowired  
    TrainingsDao dao;
       
    @RequestMapping("/trainingsform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Trainings());
    	return "trainingsform"; 
    }  
  
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("trainings") Trainings trainings){  
        dao.save(trainings);  
        return "redirect:/trainings/viewtrainings"; 
    }  
    
    @RequestMapping("/viewtrainings")  
    public String viewtrainings(Model m){  
        List<Trainings> list=dao.getTrainings();  
        m.addAttribute("list",list);
        return "viewtrainings";  
    }  
 
    @RequestMapping(value="/edittrainings/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Trainings trainings=dao.getTrainingsById(id);  
        m.addAttribute("command",trainings);
        return "trainingseditform";  
    }  
     
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("trainings") Trainings trainings){  
        dao.update(trainings);  
        return "redirect:/trainings/viewtrainings";  
    }  
      
    @RequestMapping(value="/deletetrainings/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);   
        return "redirect:/trainings/viewtrainings";  
    }   
}  