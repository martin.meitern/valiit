package com.javatpoint.controllers;   
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;

import com.javatpoint.beans.Client;
import com.javatpoint.beans.Country;
import com.javatpoint.dao.ClientDao;  

@Controller  
public class ClientController {  
    @Autowired  
    ClientDao dao; 
        
    @RequestMapping("/clientform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Client());
    	m.addAttribute("countries", countriesToMap(dao.getCountries()));
    	return "clientform"; 
    }
  
  
   
   @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("client") Client client){  
        dao.save(client);  
        return "redirect:/viewclient";
    }  

    @RequestMapping("/viewclient")  
    public String viewclient(Model m){  
        List<Client> list=dao.getClients();  
        m.addAttribute("list",list);
        return "viewclient";  
    }  
    
   @RequestMapping(value="/editclient/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Client client=dao.getClientById(id);  
        m.addAttribute("command",client);
        return "clienteditform";  
        
    }  
 
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("client") Client client){  
        dao.update(client);  
        return "redirect:/viewclient"; 
        
    }   
    
    @RequestMapping(value="/deleteclient/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/viewclient";  
       
    }   
    
    private Map<Integer, String> countriesToMap(List<Country> countries) {
    	Map<Integer, String> countriesMap = new HashMap<Integer, String>();
    	for(Country country : countries) {
    		countriesMap.put(country.getId(), country.getCountry());
    	}
    	
    	return countriesMap;
    }

}  