    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Clients</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>#</th><th>First</th><th>Last</th><th>Username</th><th>Edit</th></tr>
    <c:forEach var="client" items="${list}"> 
    <tr>
    <td>${client.id}</td>
    <td>${client.firstName}</td>
    <td>${client.lastName}</td>
    <td>${client.userName}</td>
    <td><a href="editclient/${client.id}">Edit client</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="clientform">Add Client</a>