<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Edit Client</h1>
       <form:form method="POST" action="/SpringMVCCRUDSimple/editsave">  
      	<table >  
      	<tr>
      	<td></td>  
         <td><form:hidden  path="id" /></td>
         </tr> 
         <tr>  
          <td>First Name : </td> 
          <td><form:input path="firstName"  /></td>
         </tr>  
         <tr>  
          <td>Last Name :</td>  
          <td><form:input path="lastName" /></td>
         </tr> 
         <tr>  
          <td>Username :</td>  
          <td><form:input path="userName" /></td>
         </tr> 
          <tr>  
          <td>Email :</td>  
          <td><form:input path="email" /></td>
         </tr> 
          <tr>  
          <td>Address :</td>  
          <td><form:input path="address" /></td>
         </tr> 
		 <tr>  
          <td>Country :</td>  
          <td><form:input path="country" /></td>
         </tr> 

         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
