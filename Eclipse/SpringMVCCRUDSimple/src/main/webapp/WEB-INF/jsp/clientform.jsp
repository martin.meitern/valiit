<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add Client</h1>
       <form:form method="post" action="save">  
      	<table >  
         <tr>  
          <td>First name : </td> 
          <td><form:input path="firstName"  /></td>
         </tr>  
         <tr>  
          <td>Last name :</td>  
          <td><form:input path="lastName" /></td>
         </tr> 
         <tr>  
          <td>Username :</td>  
          <td><form:input path="userName" /></td>
         </tr>
           <tr>  
          <td>Email (optional) :</td>  
          <td><form:input path="email" /></td>
         </tr>  
         <tr>  
          <td>Address :</td>  
          <td><form:input path="address" /></td>
         </tr> 
         <tr>  
          <td>Country :</td>  
          <td><form:select path="countryId" items="${countries}" /></td>
         </tr>


         <tr>  
          <td> </td>  
          <td><input type="submit" value="Add Client" /></td>  
         </tr>  
        </table>  
       </form:form>  
