package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
//	    generic arraylist of selline kollektsioon, kus objekti loomise hetkel peame määrama
//        mis tüüpi elemente see sisaldama hakkab

        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(Integer.valueOf("10"));
        numbers.add(1);
        numbers.add(2);

        List<String> words = new ArrayList<String>();
        words.add("Tere");
        words.add("Head aega");
    }
}
