package it.vali;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[] {-16, -4, -10, -1, 0, -5};
        int max = numbers[0];
        for (int i = 1; i < numbers.length ; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

//        System.out.println("Leia suurim paaritu arv. Mitmes number see nimekirjas on?");
//        int maxOdd = Integer.MIN_VALUE;
//        int item = 1;
//        boolean hasOdd = false;
//
//        for (int i = 0; i < numbers.length; i++) {
//            if (numbers[i] % 2 != 0 && numbers[i] > maxOdd) {
//                maxOdd = numbers[i];
//                item = i + 1;
//                hasOdd = true;
//            }
//        }
//        if (hasOdd) {
//            System.out.println("Suurim paaritu arv on " + maxOdd + " ja see on nimekirjas " + item + ". kohal.");
//        }
//        else {
//            System.out.println("Paaritud numbrid puudusid");
//        }
        System.out.println("Leia suurim paaritu arv. Mitmes number see nimekirjas on?");
        int maxOdd = Integer.MIN_VALUE;
        int item = -1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > maxOdd) {
                maxOdd = numbers[i];
                item = i + 1;
            }
        }
        if (item != -1) {
            System.out.println("Suurim paaritu arv on " + maxOdd + " ja see on nimekirjas " + item + ". kohal.");
        }
        else {
            System.out.println("Paaritud numbrid puudusid");
        }

        // kui paarituid arve üldse ei ole, kirjuta "Paaritud arvud puuduvad".
    }
}
