package it.vali;

import java.sql.Array;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int a = 0;
//	    esimene asi, mis vea annab, viib mind catch plokki.
        try {
            int b = 4/a;
            String word = null;
            word.length();
        }
//        Kui on mitu catch plokki, siis otsib ta esimese ploki, mis oskab
//        antud exceptioni kinni püüda
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ be zero")) {
                System.out.println("Nulliga ei saa jagada");
            }
            else {
                System.out.println("Esines aritmeetiline viga");
            }

        }
        catch (NullPointerException e) {
            System.out.println("Loll oled vä? Tühjuse pikkust ei saa mõõta");
        }
        catch (RuntimeException e) {
            System.out.println("Esines reaalajas viga");
        }
//            exception on klass, millest kõik erinevad exceptioni tüübid pärinevad
//            see omakorda tähendab, et püüdes kinni selle üldise Exceptioni
//            püüame me kinni kõik exceptionid
        catch (Exception e) {
            System.out.print("Midagi läks pekki. Ma ei tea täpselt, mis");
        }

//        Küsime kasutajalt numbri. Kui number ei ole korrektne, siis anname veateate
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;
        do {
            System.out.println("Sisesta üks number");
            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Sa ei sisetanud numbrit");
            }
        } while (!correctNumber);

//        Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        int[] numbers = new int[] {1, 2, 3, 4, 5};
        System.out.println("Sisesta indeks, kuhu soovid elemendi väärtust määrata");
        int number = Integer.parseInt(scanner.nextLine());
        try {
            numbers[number] = 4;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeksid, millele tahtsid väärtust määrata, ei mahu järjendi piiridesse");
        }
        catch (Exception e) {
            System.out.print("Midagi läks pekki. Ma ei tea täpselt, mis");
        }

    }

}
