package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean isMinor = false;
        //boolean isGrownup = false;

        System.out.println("Kui vana sa oled?");
        Scanner scanner = new Scanner(System.in);
        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;
        }
        /*else {
            isMinor = false;
        }
        */
        if(isMinor) {
            System.out.println("Sa oled alaealine");
        }
        else {
            System.out.println("Sa oled täisealine");
        }
        System.out.println("Siseta number");
        int number = Integer.parseInt(scanner.nextLine());

        boolean numberIsGreaterThan3 = number > 3;

        if ((number > 2 && number < 6) || number > 10) {
            System.out.println("Jess!");
        }

        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean f = a && b;

        if(f || c) {
            System.out.println("Endiselt JESS!");
        }
        /*
        Küsi kasutajalt kas ta on söönud hommikusööki?
        Küsi kasutajalt kas ta on söönud lõunat?
        Küsi kasutajalt, kas ta on söönud õhtusööki?
        Prindi, kas kasutaja on täna söönud või mitte
         */

        boolean hasEaten = false;
        System.out.println("Kas sa hommikusööki sõid? Kirjuta jah/ei");
        String awnser = scanner.nextLine();
        if (awnser.equals("jah")) {
            hasEaten = true;
        }
        System.out.println("Kas sa lõunasööki sõid? Kirjuta jah/ei");
        awnser = scanner.nextLine();
        if (awnser.equals("jah")) {
            hasEaten = true;
        }
        System.out.println("Kas sa lõunasööki sõid? Kirjuta jah/ei");
        awnser = scanner.nextLine();
        if (awnser.equals("jah")) {
            hasEaten = true;
        }
        if (hasEaten) {
            System.out.println("Sa oled täna söönud");
        }
        else {
            System.out.println("Sa pole söönud");
        }
    }
}
