
package it.vali;
// import tähendab, et antud Main klassile lisatakse ligipääs java class
// library paketile java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist objekti nimega "scanner", mille
        // kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Mis on sinu perekonnanimi?");
        String lastName = scanner.nextLine();

        System.out.println("Mis on sinu lemmikvärv?");
        String colour = scanner.nextLine();

        System.out.printf("Tere %s. %s on ilus värv.\n",
                name, colour);

        StringBuilder builder = new StringBuilder();
        builder.append("Tere ");
        builder.append(name);
        builder.append(". ");
        builder.append(colour);
        builder.append(" on ilus värv.");

        builder.toString();
        System.out.println(builder.toString());

        String fullText = builder.toString();
        System.out.println(fullText);

        // Nii system.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit

        String text = String.format("Tere %s. %s on ilus värv.\n",
                name, colour);
        System.out.println(text);

    }
}
