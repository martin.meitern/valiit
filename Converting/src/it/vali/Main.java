package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // küsi kasutajalt 2 numbrit ja prindi välja nende summa
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sisesta esimene number:");
        int a = Integer.parseInt(scanner.nextLine());
        System.out.println("Sisesta teine number:");
        int b = Integer.parseInt(scanner.nextLine());
        System.out.printf("Numbri %x ja %x summa on %x%n",
                a, b, a + b);

        // küsi kasutajalt 2 reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta esimene reaalarv");
        double c = Double.parseDouble(scanner.nextLine());
        System.out.println("Sisesta esimene reaalarv");
        double d = Double.parseDouble(scanner.nextLine());
        System.out.printf("Numbri %f ja %f summa on %.2f%n",
                c, d, c + d);

        System.out.println(String.format(Locale.US, "%.2f", c / d));

    }
}
