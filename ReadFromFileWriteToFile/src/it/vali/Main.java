package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
	// loe failist input.txt iga teine rida ning kirjuta need read faili output.txt
        try {
//            Notepad vaikimisi kasutab ANSI kodeeringut.
//            Selleks, et fileReader oskaks seda korrektselt lugeda (täpitähti),
//            peame talle ette ütlema et loe seda ANSI kodeeringus
//            Cp1252 on javas ANSI kodeering
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
//            Faili kirjutades kasutab java automaatselt UTF-8 kodeeringut ja seda ei pea eraldi määrama
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"));

            String line = bufferedReader.readLine();
            int i = 0;
            while (line != null) {
                if (i%2==0) {
                    fileWriter.write(line + System.lineSeparator());
                }
                i++;
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            fileReader.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
