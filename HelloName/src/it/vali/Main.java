package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi(String) muutuja (variable)
        // mille nimeks paneme "name" ka väärtuseks "Martin"
	    String name = "Martin";
	    String lastName = "Meitern";
	    System.out.println("Hello " + name + " " + lastName);
    }
}
