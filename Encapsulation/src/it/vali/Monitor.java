package it.vali;

//enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
//Tegelikult salvestatakse enum alati int-na
enum Color {
    BLACK,
    WHITE,
    GREY
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

public class Monitor {
    private String manufacturer;
    private double diagonal; //tollides "
    private Color color;
    private ScreenType screenType;

    public int getYear() {
        return year;
    }

    private int year = 2000;

    public double getDiagonal() {
        if (diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }

    public void setDiagonal(double diagonal) {
//        this tähistab seda konkreetset objekti
        if (diagonal < 0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        }
        else if (diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem, kui 100\"");
        }
        else {
            this.diagonal = diagonal;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
// kui ekraanitüüp on seadistamata (null), siis tagastab tüübiks LCD
    public ScreenType getScreenType() {
        if (screenType == null) {
            return ScreenType.LCD;
        }
        else {
            return screenType;
        }
    }

    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }

    public String getManufacturer() {
        return manufacturer;
    }
//      ära lubada seadistada monitori tootjaks Huawei
//    Kui keegi soovib seda tootjat monitori tootjaks seadistada, pannakse tootjaks tekst "Tootja puudub"
//    Keela ka tühja tootja nime lisamine
    public void setManufacturer(String manufacturer) {
        if (manufacturer == null || manufacturer.toLowerCase().equals("huawei") || manufacturer.equals("")) {
            this.manufacturer = "Tootja puudub";
        }
        else {
            this.manufacturer = manufacturer;
        }
    }

    public void printInfo() {

        System.out.println("Monitors info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType());
        System.out.printf("Aasta: %s%n", year);
    }

//    tee meetod, mis tagastab ekraani diagonaali sentimeetrites
    public double diagonalToCm() {
        return diagonal * 2.54;
    }
}
