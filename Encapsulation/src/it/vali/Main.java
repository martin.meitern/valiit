package it.vali;

public class Main {

    public static void main(String[] args) {
//        vaikeväärtused
//        int = 0
//        boolean = false
//        Double = 0.0
//        String = null
//        objektid (Monitor) = null
//        massiiv = null


        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(1000);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(-10);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());

        firstMonitor.setManufacturer("Huawei");
        firstMonitor.printInfo();
    }
}
