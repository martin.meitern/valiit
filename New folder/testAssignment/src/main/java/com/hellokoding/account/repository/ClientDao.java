package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Client;
import com.hellokoding.account.model.Country;  
  
public class ClientDao {  
	JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Client p){  
	    String sql="insert into client(firstname,lastname,username,email,address,countryid,userId) values('"+p.getFirstName()+"','"+p.getLastName()+"','"+p.getUserName()+"','"+p.getEmail()+"','"+p.getAddress()+"','"+p.getCountryId()+"', "+p.getUserId()+")";  
	    return template.update(sql);  
	} 
	public int update(Client p){  
	    String sql="update client set firstname='"+p.getFirstName()+"', lastname='"+p.getLastName()+"',username='"+p.getUserName()+"',email='"+p.getEmail()+"',address='"+p.getAddress()+"',countryId="+p.getCountryId() + " where id="+p.getId()+"";;  
	    return template.update(sql);  
	}  
	
	public int delete(int id){  
	    String sql="delete from client where id="+id+"";  
	    return template.update(sql);  
	}  
	public Client getClientById(int id){  
	    String sql="SELECT * FROM client WHERE id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Client>(Client.class));  
	}  
	public List<Client> getClients(long userId){  
	    return template.query("SELECT \r\n" + 
	    		"cl.id '\\#',\r\n" + 
	    		"cl.firstName, \r\n" + 
	    		"cl.lastName,\r\n" + 
	    		"cl.userName\r\n" + 
	    		"FROM client AS cl WHERE userId=" +userId,new RowMapper<Client>(){  
	        public Client mapRow(ResultSet rs, int row) throws SQLException {  
	        	Client c=new Client();  
	            c.setId(rs.getInt(1));  
	            c.setFirstName(rs.getString(2));  
	            c.setLastName(rs.getString(3));   
	            c.setUserName(rs.getString(4));  
	            return c;  
	        }  
	    });  
	}
	
	public List<Country> getCountries(){  
	    return template.query("select id,country from country",new RowMapper<Country>(){  
	        public Country mapRow(ResultSet rs, int row) throws SQLException {  
	            Country country=new Country();  
	            country.setId(rs.getInt(1));  
	            country.setCountry(rs.getString(2));  
	            return country;  
	        }  
	    });  
	}

	
}  