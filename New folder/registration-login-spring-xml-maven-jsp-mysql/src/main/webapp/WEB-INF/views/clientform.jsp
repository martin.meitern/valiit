<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!-- <h1>Add Client</h1>
       <form:form method="post" action="save">  
      	<table >  
         <tr>  
          <td>First name : </td> 
          <td><form:input path="firstName"  /></td>
         </tr>  
         <tr>  
          <td>Last name :</td>  
          <td><form:input path="lastName" /></td>
         </tr> 
         <tr>  
          <td>Username :</td>  
          <td><form:input path="userName" /></td>
         </tr>
           <tr>  
          <td>Email (optional) :</td>  
          <td><form:input path="email" /></td>
         </tr>  
         <tr>  
          <td>Address :</td>  
          <td><form:input path="address" /></td>
         </tr> 
         <tr>  
          <td>Country :</td>  
          <td><form:select path="countryId" items="${countries}" /></td>
         </tr>


         <tr>  
          <td> </td>  
          <td><input type="submit" value="Add Client" /></td>  
         </tr>  
        </table>  
       </form:form>   -->
		
       
       
       <!DOCTYPE html>
<html lang="en">
<head>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit client</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
   
    
    
    
    
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bootstrap CRUD Data Table for Database with Modal Form</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">

	
  body {
        color: #566787;
		background: #f5f5f5;
		font-family: 'Roboto Expanded', sans-serif;
		font-size: 15px;
		
	}

	</style>
	<script type="text/javascript">
	$(document).ready(function(){
		// Activate tooltip
		$('[data-toggle="tooltip"]').tooltip();
		
		// Select/Deselect checkboxes
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function(){
			if(this.checked){
				checkbox.each(function(){
					this.checked = true;                        
				});
			} else{
				checkbox.each(function(){
					this.checked = false;                        
				});
			} 
		});
		checkbox.click(function(){
			if(!this.checked){
				$("#selectAll").prop("checked", false);
			}
		});
	});
	</script>
	    
    
    
    
    
</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
		



    <div class="container" style="width:60%">
        <div class="table-wrapper">
            <div class="table-title" >
                <div class="row">
                    <div class="col-sm-6">
						<h2>Add <b>client</b></h2>
					</div>
                </div>
            </div>
            
            
             <c:if test="${pageContext.request.userPrincipal.name != null}">
		        <form id="logoutForm" method="POST" action="${contextPath}/logout">
		            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		        </form>
			</c:if>
            
            
            <br>
            
            <h4><b>Client</b></h4>
		      <form:form method="POST" action="/account/client/save">  
		     	<table class="table table-striped table-hover">  
		     	<tr >
		     	<td></td>  
		        <td><form:hidden  path="id" /></td>
		        </tr> 		  			  
		        <tr>  
			          <td>First Name : </td> 
			          <td><form:input path="firstName" required="required" /></td>
			         </tr>  
			         <tr>  
			          <td>Last Name :</td>  
			          <td><form:input path="lastName" required="required" /></td>
			         </tr> 
			         <tr>  
			          <td>Username :</td>  
			          <td><form:input path="userName" placeholder="Username" required="required" /></td>
			         </tr> 
			          <tr>  
			          <td>Email (optional) :</td>  
			          <td><form:input path="email" placeholder="you@email.com" /></td>
			         </tr> 
			          <tr>  
			          <td>Address :</td>  
			          <td><form:input path="address" placeholder="1600 Pennsylvania Ave NW, Washington, DC 20500, USA" required="required" /></td>
			         </tr> 
			         <tr>  
			          <td>Country :</td>  
			          <td><form:select path="countryId" items="${countries}" /></td>
			         </tr>
			           <tr>  
			          <td> </td>  
			          <td><input type="submit" value="Add Client" /></td>  
			         </tr>  
			         
			      	 </table>

         
  
        
      </form:form>  
      
      
      
      
            
            	
    <br/>
    <c:if test="${isAdmin}"><a href="employeeform">Add New Employee  | </a></c:if>

	           
            
            
		
        </div>
    </div>
	
	
  <!-- Modal -->

		<div class="container">
		  				
		  <div class="modal fade" id="myModal" role="dialog" style="layout:center">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		       
		      </div>
		      
		    </div>
		  </div>
		  
		</div>
		
		
		<div class="container">
		  				
		  <div class="modal fade" id="myModalTwo" role="dialog" style="layout:center">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4>
		        </div>
		        <div class="modal-body">
		          <p>Some text in the modal.</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		  
		</div>
	
	
	
</body>
</html>
