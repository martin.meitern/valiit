package it.vali;

public class Main {

    public static void main(String[] args) {

//	    For-loop on selline tsükkel, kus korduste arv on teada
//
//	    lõpmatu tüskkel
//	    for(;;) {
//            System.out.println("Väljas on ilus ilm");
//        }
//      1) int i = 0
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }
//        Prindi ekraanile numbrit 1-10
        for (int i = 0; i <= 10; i++) {
            System.out.println(i);

        }
        System.out.println();
        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }
        System.out.println();
        for (int i = 18; i >= 3 ; i--) {
            System.out.println(i);
        }
        System.out.println();
        for (int i = 2; i <= 10; i+= 2) {
            System.out.println(i);
        }
//        Prindi ekraanile numbrid 10-20 ja 40-60
        for (int i = 10; i <= 60; i++) {
            if (i < 21 || i > 39) {
                System.out.println(i);
            }


            System.out.println();

        }
        for (int i = 0; i <= 60; i++) {
            if (i == 21) {
                i = 40;
            }
            System.out.println(i);
        }

        // prindi kõik arvud, mis jaguvad kolmega vahemikus 10 - 50
        for (int i = 10; i <= 50; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }


    }
}
