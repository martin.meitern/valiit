package it.vali;

public class Main {

    public static void main(String[] args) {
//        Abstraktne klass on hübriid liidesest ja klassist
//        Selles klassis saab defineerida nii meetodi struktuure (nagu liideses(interface'is), aga saab ka
//        defineerida meetodi koos sisuga.
//        Abstraktsest klassist ei saa otse objekti luua, saab ainult pärineda

        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(100);
        kitchen.becomeDirty();

    }
}
