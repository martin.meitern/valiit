package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta e-mail");
        Scanner scanner = new Scanner(System.in);

        String email = scanner.nextLine();
        String emailRegex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";
        if (email.matches(emailRegex)) {
            System.out.println("E-mail oli korrektne");
        }
        else {
            System.out.println("E-mail ei olnud korrektne");
        }
        Matcher matcher = Pattern.compile(emailRegex).matcher(email);
        if(matcher.matches()) {
            System.out.println("Kogu e-mail: " + matcher.group(0));
            System.out.println("Tekst vasakulpool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }

//        Küsi kasutajalt isikukood ja valideeri, kas see on õiges formaadis ning prindi välja tema sünnipäev
        System.out.println("Sisesta isikukood");
        String idNumber = scanner.nextLine();
        String idRegex = "^([3-4]([0-9]{2})|[5-6](0[0-9]|1[0-9]))(1[0-2]|0[1-9])(0[1-9]|[12][0-9]|3[01])[0-9]{4}$";
        if (idNumber.matches(idRegex)) {
            System.out.println("Isikukood on korrektne");
        }
        else {
            System.out.println("Isikukood ei ole korrektne");
        }
        matcher = Pattern.compile(idRegex).matcher(idNumber);
        if(matcher.matches()) {
            System.out.printf("sünnipäev on %s.%s.%s%n", matcher.group(5), matcher.group(4), matcher.group(2));
//            System.out.println("See inimene on sündinud " + matcher.group(2) + " kuupäeval");
        }
    }
}