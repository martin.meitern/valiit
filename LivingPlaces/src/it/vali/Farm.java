package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace{

    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();

//    Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

//    Siin hoitakse infot, palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Farm() {
        maxAnimalCounts.put("Sheep", 15);
        maxAnimalCounts.put("Cow", 2);
        maxAnimalCounts.put("Pig", 5);
    }
    @Override
    public void addAnimal(Animal animal) {
//        Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if (!FarmAnimal.class.isInstance(animal)) {
            System.out.println("Farmis saavad elada ainult farmiloomad");
            return;
        }

        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis sellistele loomadele kohta ei ole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
        }
        else {
//            Siin on need loomad, kes mahuvad farmi, aga keda praegu pole seal
            animalCounts.put(animalType, 1);
        }
        animals.add((FarmAnimal) animal);
        System.out.printf("Farmi lisati %s%n", animalType);
    }


//    Tee meetod, mis prindib välja kõik farmis elavad loomad ning mitu neid on
//    Tee meetod, mis eemaldab farmist looma
    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry: animalCounts.entrySet()) {
            System.out.printf("Farmis on %d %s.%n", entry.getValue(), entry.getKey());
        }
    }
    @Override
    public void removeAnimal(String animalType) {

        boolean animalFound = false;
        for (FarmAnimal animal:
             animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Farmist eemaldati 1 %s%n", animalType);

//                Kui see oli viimane omasugune loom, siis eemalda rida animalCounts mapist
//                muul juhul vähenda animalCounts mapist

                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                }
                else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if (!animalFound) {
            System.out.println("Farmis pole enam ühtegi sellist looma");
        }
    }
}
