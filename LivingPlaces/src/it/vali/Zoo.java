package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {

    private List<Animal> animals = new ArrayList<Animal>();

    //    Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Lion",6);
        maxAnimalCounts.put("Elephant", 2);
        maxAnimalCounts.put("Horse", 5);
    }

    @Override
    public void addAnimal(Animal animal) {

        if (Pet.class.isInstance(animal)) {
            System.out.println("Loomaaias ei ela koduloomad");
            return;
        }
        String animalType = animal.getClass().getSimpleName();

        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaaias sellistele loomadele kohta ei ole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if (animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
        }
        else {
//            Siin on need loomad, kes mahuvad farmi, aga keda praegu pole seal
            animalCounts.put(animalType, 1);
        }
        animals.add(animal);
        System.out.printf("Loomaaeda lisati %s%n", animalType);
    }

    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry: animalCounts.entrySet()) {
            System.out.printf("Farmis on %d %s.%n", entry.getValue(), entry.getKey());
        }
    }

    @Override
    public void removeAnimal(String animalType) {
        boolean animalFound = false;
        for (Animal animal:
                animals) {
            if (animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Loomaaiast eemaldati 1 %s%n", animalType);

//                Kui see oli viimane omasugune loom, siis eemalda rida animalCounts mapist
//                muul juhul vähenda animalCounts mapist

                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                }
                else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if (!animalFound) {
            System.out.println("Loomaaias pole enam ühtegi sellist looma");
        }
    }
}
