package it.vali;

public class WildAnimal extends Animal {
    public static boolean isSleepsDuringWinter() {
        return sleepsDuringWinter;
    }

    public static void setSleepsDuringWinter(boolean sleepsDuringWinter) {
        WildAnimal.sleepsDuringWinter = sleepsDuringWinter;
    }

    private static boolean sleepsDuringWinter;

    public void findsFood() {
        System.out.println("Otsin iseendale toitu");
    }

    @Override
    public String getName() {
        return "metsloomal pole nime";
    }
}
