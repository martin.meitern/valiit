package it.vali;

public class Main {

    public static void main(String[] args) {
        LivingPlace livingPlace = new Farm();
        Pig pig = new Pig();
        livingPlace.addAnimal(pig);
        Cow cow = new Cow();

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Chicken());
        livingPlace.addAnimal(new Cow());
        livingPlace.addAnimal(cow);
        livingPlace.addAnimal(new Cow());
        livingPlace.addAnimal(new Cow());
        System.out.println();
        livingPlace.printAnimalCounts();
        System.out.println();
        livingPlace.removeAnimal("Pig");
        livingPlace.removeAnimal("Cow");
        System.out.println();
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Cow");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Cow");

//        täienda zoo ja forest klasse nii, et neil oleks 3 meetodi sisu

        System.out.println("Loomaaed");
        LivingPlace zoo = new Zoo();
        zoo.removeAnimal("Monkey");
        zoo.addAnimal(new Lion());
        zoo.addAnimal(new Lion());
        zoo.addAnimal(new Elephant());
        zoo.addAnimal(new Elephant());
        zoo.addAnimal(new Elephant());
        zoo.printAnimalCounts();
        zoo.removeAnimal("Elephant");
        zoo.addAnimal(new Dog());

    }
}
