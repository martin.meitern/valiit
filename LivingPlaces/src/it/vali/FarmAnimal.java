package it.vali;

public class FarmAnimal extends DomesticAnimal {
    public String getProduce() {
        return produce;
    }

    public void setProduce(String produce) {
        this.produce = produce;
    }

    private String produce;

    public void produce(String product) {
        System.out.printf("Säh, võta %s!%n", product);
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Mind kasvatatakse, sest et: %s%n", produce);
    }
}
