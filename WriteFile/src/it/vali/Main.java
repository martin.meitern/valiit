package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
//        try plokis otsitakse/oodatakse exceptioneid (erind, erand, viga)
        try {
//            FileWriter on selline klass, mis tegeleb faili kirjutamisega.
//            Sellest klassist objekti loomisel antakse talle ette faili asukoht
//            Faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt
//            (sel juhul kirjutatakse fail samasse kausta, kus asub Main.class)
//            või siis täispika asukohaga c:\\users\\opilane\\documents\\output.txt
//
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            fileWriter.write("Elas metsas mutionu" + System.lineSeparator());
            fileWriter.write(String.format("Elas metsas mutionu%n"));
            fileWriter.write("Elas metsas mutionu\r\n");
            fileWriter.close();

//            catch plokis püütakse kinni kindlat tüüp exception
//            või kõik exceptionid, mis pärinevad antud exceptionist
        } catch (IOException e) {
//            printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise
//            hierarhia/ajalugu. See tuleks kliendile andes välja võtta ja kirjutada mis viga tekkis
//            e.printStackTrace();
            System.out.println("Viga: ligipääs sellele failile ei ole võimalik");
        }

    }
}
