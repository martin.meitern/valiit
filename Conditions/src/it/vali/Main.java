package it.vali;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta number");
        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());


        /* Java on type-sade language
        ma ei saa teha järgmist koodi:
        a = 2.33
        a = "tere"
         */
        if(a == 3){
            System.out.printf("Su kirjutatud number %d on 3%n", a);
        }
        else if (a < 5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
        }
        else {
            System.out.printf("Arv %d ei ole kolm ja on 5 või suurem viiest%n", a);
        }
        if (a != 6) {
            System.out.printf("Arv %d ei võrdu kuuega%n", a);
        }
        else {
            System.out.printf("Arv %d on 6!%n", a);
        }

        if (!(a >= 7)) {
            System.out.printf("Arv %d on väiksem seitsmest%n", a);
        }
        /*
        Kui tingimuste vahel on JA, siis kui mingi tingimus on vale, siis järgmisi tingimusi
        enam ei kontrollita. Sellepärast võiks ettepoole panna selle tingimuse,
        mis kõige rohkem variante välistab. Sama kehtib ka VÕI kohta
         */
        if (a > 2 && a < 8) {
            System.out.printf("%d on suurem kui 2 ja väiksem kui 8%n", a);
        }

        if (a < 2 || a > 8) {
            System.out.printf("%d on väiksem kui 2 või suurem kui 8%n", a);
        }

        if ((a > 2 && a < 8) || (a > 5 && a < 8) || (a > 10)) {
            System.out.printf("%d on kahe ja kaheksa või viie ja kaheksa vahel või arv on suurem kui 10%n", a);
        }
        if ((!(a > 4 && a < 6) && a>5 && a < 8) || (a < 0 && !(a > -14))){
            System.out.printf("Kui %d ei ole 4 ja 6 vahel, aga on 5 ja 8 vahel%n", a);
        }

    }
}
