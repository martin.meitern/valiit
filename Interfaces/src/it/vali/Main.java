package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

        System.out.println();
        for (Flyer flyer:
             flyers) {
            flyer.fly();
            System.out.println();
        }

//        Lisa liides Driver, klass Car. Mõtle, kas lennuk ja auto võiks mõlemad kasutada Driver liidest?
//        Driver liides võiks sisaldada 3 meetodi kirjeldust:
//        1) int getMaxDistance()
//        2) void drive()
//        3) void stopDriving(int afterDistanceA)
//        Pane auto ja lennuk ja mõlemad kasutama seda liidest
//        + mootorratta klass

        Car car = new Car(600);
        car.drive();
        car.stopDriving(10);
        System.out.println(car.getMaxDistance());

        car.setMake("Audi");
        System.out.println(car);

        System.out.println(boeing);

    }
}
