package it.vali;

public class Car extends Vehicle implements Driver {

    private int maxDistance;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Car(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public Car() {
        this.maxDistance = 600;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        startEngine();
        System.out.println("Auto sõidab");
    }

    @Override
    public String toString() {
        return make;
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lõpetan sõitmise %d meetri pärast %n", afterDistance);
        stopEngine();
    }
    private void stopEngine() {
        System.out.println("Mootor seiskus");
    }
    private void startEngine() {
        System.out.println("Mootor käivitus");
    }
}
