package it.vali;

//  Interface ehk liides sunnib seda kasutavat/implementeerivat klassi omama
//  liideses kirja pandud meetodeid (sama tagastuse tüübiga ja sama parameetrite kombinatsiooniga)

//  Iga klass, mis interface'i kasutab, määrab ise ära meetodi sisu

public interface Flyer {
    void fly();
}
