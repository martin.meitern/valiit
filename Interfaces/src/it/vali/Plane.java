package it.vali;

//  klass Plane pärineb klassist Vehicle ja kasutab/implementeerib liidest Flyer
public class Plane extends Vehicle implements Flyer, Driver {

    private int maxDistance = 2000;

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doCheckList() {
        System.out.println("Täidetakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Lennuk sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lõpetan sõitmise %d meetri pärast %n", afterDistance);
    }
}
