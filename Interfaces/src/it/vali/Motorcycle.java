package it.vali;

public class Motorcycle extends Vehicle implements DriverInTwoWheels {

    int maxDistance = 700;

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        startEngine();
        System.out.println("Mootorratas sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lõpetan sõitmise %d meetri pärast %n", afterDistance);
        stopEngine();
    }

    private void stopEngine() {
        System.out.println("Mootor seiskus");
    }
    private void startEngine() {
        System.out.println("Mootor käivitus");
    }

    @Override
    public void driveOnRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal");
    }
}
