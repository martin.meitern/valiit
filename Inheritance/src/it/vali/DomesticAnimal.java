package it.vali;

public class DomesticAnimal extends Animal {
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    private String ownerName;

    public void isGivenFood() {
        System.out.println("Aitäh toidu eest, pai peremees");
    }
}
