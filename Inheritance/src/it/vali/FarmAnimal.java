package it.vali;

public class FarmAnimal extends DomesticAnimal {
    public String getProduce() {
        return produce;
    }

    public void setProduce(String produce) {
        this.produce = produce;
    }

    private String produce;

    public void produce(String product) {
        System.out.printf("Säh, võta %s!%n", product);
    }
}
