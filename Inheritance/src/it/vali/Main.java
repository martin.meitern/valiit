package it.vali;

public class Main {

    public static void main(String[] args) {
	Cat angora = new Cat();
	angora.setAge(10);
	angora.setBreed("Angora");
	angora.setName("Miisu");
	angora.setWeight(2.34);

	angora.printInfo();
	angora.eat();

	System.out.println();

	Cat persian = new Cat();
	persian.setAge(1);
	persian.setBreed("Persian");
	persian.setName("Liisu");
	persian.setWeight(3.11);

	persian.printInfo();
	persian.eat();

	System.out.println();

	Dog tax = new Dog();
	tax.setAge(3);
	tax.setBreed("Tax");
	tax.setName("Muki");
	tax.setWeight(3.22);

	tax.printInfo();
	tax.eat();

	persian.catchMouse();
	tax.playWithCat(persian);

	System.out.println();

	FarmAnimal cow = new FarmAnimal();
	cow.setName("Alma");
	cow.setAge(5);
	cow.setBreed("Hereford");
	cow.setWeight(256);
	cow.setProduce("Milk");
	cow.setOwnerName("Andres");

	cow.produce("piim");
	cow.isGivenFood();
	cow.eat();
	cow.printInfo();

//	Lisa pärinevusahelast puuduvad klassid
//	Igale klassile lisa üks muutuja ja üks meetod, mis on ainult sellele klassile omane
	}
}
