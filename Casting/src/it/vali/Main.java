package it.vali;

public class Main {

    public static void main(String[] args) {
	// Casting on teisendamine ühest arvutüübist teise
        byte a = 3;
        /* kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine
        ehk implicit casting
         */
        short b = a;

        /* Kui üks numbritüüp ei mahu teise sisse, siis peab kõigepealt
        ise veenduma, et number mahub teise numbritüüpi ja kui mahub, siis
        peab ise seda teisendama. See on Explicit casting
         */

       // byte c = (byte)b;
        short c = 300;
        byte d = (byte)c;
        System.out.println(d);

        long e = 10000000000L;
        int f = (int) e;
        System.out.println(f);

        float h = 123.23424F;
        double i = h;


        /* Kui suurem number (või rohkemate komakohtadega number) teisendada väiksemaks,
        siis java kõigepealt küsib, kas tahame teisendada. Ja seejärel võtab ära komakohad
         */
        double j = 55.1234567891;
        float k = (float) j;
        System.out.println(k);

        double l = 12E50;
        float m = (float) l;
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;

        System.out.println(q);

        int r = 2;
        double s = 9;
        /* int + int = int
            int + double = double
            double + int = double
            double + double = double
         */
        System.out.println(r / s);

        float t = 12.14251F;
        double u = 23.54534;
        System.out.println(t/u);
    }
}
