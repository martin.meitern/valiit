// public 				- klass, meetod või muutuja on
//                        avalikult nähtav/ligipääsetav
// class 				- Javas üksus, üldiselt ka eraldi fail,
//                        mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld 			- klassi nimi, mis on ka faili nimi
// static				- meetodi ees tähendab, et seda meetodit saab
//                        välja kutsuda ilma klassist objekti loomata
// void					- meetod ei tagasta midagi
// main					- (meetodi tüüp) meetodile on võimalik kaasa anda parameetrid,
//                         mis pannakse sulgude sisse, eraldades komaga
//String[] 				- tähistab stringi massiivi
// args 				- massiivi nimi, sisaldab käsuirealt kaasa pandud parameetreid
// System.out.println 	- Java meetod, millega saab välja printida rida teksti.
//                        See, mis kirjutatakse sulgudesse, prinditakse välja ja tehakse reavahetus

package it.vali;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
