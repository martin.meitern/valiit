package it.vali;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//	      1. Arvuta ringi pindala, kui teada on raadius. Prindi pindala välja ekraanile

//        2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisend parameetriteks on kaks Stringi.
//        Meetod tagastab, kas true või false selle kohta, kas Stringid on võrdsed.

//        3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab Stringide massiivi.
//        Iga sisendmassiivi elemendi kohta olgu tagastatavas massiivis sama palju a-tähti (1, 2, 3) -> ("a", "aa", "aaa")

//        4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja tagastab kõik sellel sajandil esinenud liigaastad.
//        Sisestada saab aastaid vahemikus 500-2019.

//        5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikidest,
//        kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii,
//        et see tagastab riikide nimekirja eraldades komaga.
//        Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning
//        prindi välja selle objekti toString() meetodi sisu. return toString()

        circleArea(4);

        System.out.println(isEqual("Maja", "maja"));

        int[] array = new int[]{1, 2, 3};

//        stringArray(array);

        int a = 2000;

        System.out.println(Arrays.toString(stringArray(array)));

//        for (int year:
//        leapYearsInCentury(1897)) {
//        }

        List<Integer> years = leapYearsInCentury(1987);
        for (int year: years
             )
            System.out.println(years);{

        }


        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");
        countryNames.add("India");

        language.setCountryNames(countryNames);

        System.out.println(language.toString());


    }

    static void circleArea(int radius) {
        System.out.printf("Ringi raadiusega %d pindala on %.2f%n", radius, Math.PI * radius * radius);
    }

    static boolean isEqual(String a, String b) {
        if (a.toLowerCase().equals(b.toLowerCase())){
            return true;
        }
        return false;

    }

//    static String[] stringArray(int[] numbers) {
//        String[] words = new String[numbers.length];
//        for (int i = 0; i < numbers.length; i++) {
//            words[i] = "a".repeat(numbers[i]);
//        }
//        return words;
//    }

    static String[] stringArray(int[] numbers) {
        String[] words = new String[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            words[i] = generateAString(numbers[i]);
        }
        return words;
    }


    static String generateAString(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word += "a";
        }
        return word;
    }

//        4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja tagastab kõik sellel sajandil esinenud liigaastad.
//        Sisestada saab aastaid vahemikus 500-2019.

    static boolean isLeapYear(int year) {
        if(year % 4 != 0)
        {
            return false;
        }
        else if(year % 100 == 0 && year % 400 != 0)
        {
            return false;
        }
        else if(year % 400 == 0)
        {
            return true;
        }
        else
        {
            return true;
        }
    }

    static List<Integer> leapYearsInCentury(int year) {
        List<Integer> years = new ArrayList<Integer>();

        if(year< 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart ; i -= 4) {
            if(i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }
        return years;
    }

//        5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikidest,
//        kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii,
//        et see tagastab riikide nimekirja eraldades komaga.
//        Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning
//        prindi välja selle objekti toString() meetodi sisu. return toString()

}
