package it.vali;

import java.util.Random;
import java.util.Scanner;

import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
//        while(true) {
//            System.out.println("Tere");
//        }
//        Lõpmatu tsükkel

//        Programm mõtleb ühe numbri ja palub kasutajal number ära arvata.
//        Senikaua, kuni kasutaja arvab valesti, ütleb "proovi uuesti".

//        While-tsüklis ei ole korduste arv teada

        Random random = new Random();
        int a = random.nextInt(5) +1;

        int number = ThreadLocalRandom.current().nextInt(1, 6 + 1);
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Mõtlesin ühe numbri peale ühest kuueni. Arva see number ära!");
            int userNumber = Integer.parseInt(scanner.nextLine());
            while (number != userNumber) {
                System.out.println("Vale number! Arva uuesti.");
                if (userNumber > number) {
                    System.out.println("Su pakutud number on suurem.");
                }
                else {
                    System.out.println("Su pakutud number on väiksem.");
                }
                userNumber = Integer.parseInt(scanner.nextLine());
            }
            System.out.println("Tubli laps.");
            System.out.println("Kas soovid eel mängida? Jah/ei");
        } while (!scanner.nextLine().toLowerCase().equals("ei"));


    }
}
