package it.vali;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

//        Oletame, et tahan teada, kuidas on inglise keeles "puu"

//        Map järjekorda ei salvesta
//        LinkedHashMap salvestab ka järjekorra

        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberName = new HashMap<String, String>();

        idNumberName.put("39008220222", "Rauno");
        idNumberName.put("39205220210", "Kalle");
        idNumberName.put("39205220210", "Joonas");

//        Kui kasutada put sama võtme lisamisel, kirjutatakse väärtus üle

        System.out.println(idNumberName.get("39205220210"));

        idNumberName.remove("39205220210");
        System.out.println(idNumberName.get("39205220210"));

//        Loe lauses üle kõik erinevad tähed ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli


//        Char on selline tüüp, kus saab hoida üksikut sümbolit

        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';

        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if (letterCounts.containsKey(characters[i])) {
                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1);
            }
            else {
                letterCounts.put(characters[i], 1);
            }
        }
//        e - 2
//        l - 1
//        a - 2
//        s - 3

//        Map.Entry<Character,Integer> on klass, mis hoiab endas ühte rida Map'is ehk siis ühte key-value paare
        for (Map.Entry<Character, Integer> entry:
             letterCounts.entrySet()) {
            System.out.printf("Tähte '%s' esines lauses %d korda.%n", entry.getKey(), entry.getValue());
        }

        Map<String, String> linkedMap = new LinkedHashMap<>();

        linkedMap.put("Maja", "House");
        linkedMap.put("Isa", "Dad");
        linkedMap.put("Puu", "Tree");
        linkedMap.put("Sinine", "Blue");

    }
}
