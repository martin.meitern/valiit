package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//	küsi kasutajalt 2 arvu. Seejärel küsi kasutajalt, mis tehet ta soovib teha ( a) liitmine, b) lahutamine, c) korrutamine, d) jagamine)
//        Prindi kasutajale vastus
        Scanner scanner = new Scanner(System.in);
        do {System.out.println("Sisesta esimene arv:");
            int firstNumber = Integer.parseInt(scanner.nextLine());
            System.out.println("Sisesta teine arv:");
            int secondNumber = Integer.parseInt(scanner.nextLine());
            boolean correctOperation = false;
            do {
                System.out.println("Millist tehed soovid teha?");
                System.out.println("a) liitmine");
                System.out.println("b) lahutamine");
                System.out.println("c) korrutamine");
                System.out.println("d) jagamine");
                String operation = scanner.nextLine();
                if (operation.equals("a")) {
                    System.out.printf("Arvude %d ja %d summa on %d%n", firstNumber, secondNumber, sum(firstNumber, secondNumber));
                    correctOperation = true;
                }
                else if (operation.equals("b")) {
                    System.out.printf("Arvude %d ja %d vahe on %d%n", firstNumber, secondNumber, subtract(firstNumber, secondNumber));
                    correctOperation = true;
                }
                else if (operation.equals("c")) {
                    System.out.printf("Arvude %d ja %d korrutis on %d%n", firstNumber, secondNumber, multiply(firstNumber, secondNumber));
                    correctOperation = true;
                }
                else if (operation.equals("d")) {
                    System.out.printf("Arvude %d ja %d jagatis on %.2f%n", firstNumber, secondNumber, divide(firstNumber, secondNumber));
                    correctOperation = true;
                }
                else {
                    System.out.println("Selline tehe puudub");
                }
            } while (!correctOperation);
            System.out.println("Kas tahad uuesti arvutada? jah/ei");
        } while (!scanner.nextLine().toLowerCase().equals("ei"));
    }
    static int sum(int a, int b) {
        return a + b;
    }
    static int subtract(int a, int b){
        return a - b;
    }
    static int multiply(int a, int b) {
        return a * b;
    }
    static double divide(double a, double b) {
        return (double)a / b;
    }

}
