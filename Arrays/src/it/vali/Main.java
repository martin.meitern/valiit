package it.vali;

public class Main {

    public static void main(String[] args) {
        //Järjend/massiiv/nimekiri
        // saad ühes muutujas hoida sama tüüpi elemente
        //luuakse näidisarvude massiiv, millesse mahub 5 elementi
        int[] numbers = new int[5];
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;
        //    System.out.println(numbers[3]);

//        for (int i : numbers) {
//            System.out.println(i);
//        }

        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
        System.out.println("Prindi välja kõik arvud, mis on suuremad, kui 2");

        for (int i = 0; i < 5; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println("Prindi välja kõik paaris arvud");

        for (int i = 0; i < 5; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println("Prindi välja 2 tagantpoolt esimest paaritut arvu");

        int counter = 0;
        for (int i = 4; i >= 0; i--) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter == 2){
                    break;
                }
            }
        }
        System.out.println("Loo uus järjend esimese järjendi 3 esimese elemendiga");
        int[] numbersTwo = new int[3];
        for (int i = 0; i < numbersTwo.length; i++) {
            numbersTwo[i] = numbers[i];
        }
        for (int i : numbersTwo) {
            System.out.println(i);
        }
        System.out.println("Loo kolmas järjend esimese järjendi 3 viimase elemendiga");
        int[] numbersThree = new int[3];
        for (int i = 0; i < numbersThree.length; i++) {
            numbersThree[i] = numbers[numbers.length - i - 1];
        }
        for (int i : numbersThree) {
            System.out.println(i);
        }
        System.out.println("Loo neljas järjend esimese järjendi 4 viimase elemendiga üle ühe elemendi");
        int[] numbersFour = new int[4];
        for (int i = 0, j = numbers.length - 1; i < numbersTwo.length; i++, j-=2) {
            numbersFour[i] = numbers[j];
        }
        for (int i : numbersFour) {
            System.out.println(i);
        }
    }

}
