package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


//	    System.out.println("Kas tahad jätkata? Jah/ei");
//
////	    Jätkame seni, kuni kasutaja kirjuta "ei"
//        Scanner scanner = new Scanner(System.in);
//        String anwser = scanner.nextLine();
//        while (!anwser.equals("ei")) {
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            anwser = scanner.nextLine();


//        Do-while tsükkel on nagu While tsükkel,
//        aga kontroll tehakse pärast esimest kordust

        Scanner scanner = new Scanner(System.in);
        String answer;
        //Iga muutuja, mille deklareerime, kehtib ainult seda
        //ümbritsevate {} sees
        do {
            System.out.println("Kas tahaksid jätkata?");
            answer = scanner.nextLine();
        }
        while (!answer.equals("ei"));
        }
    }


