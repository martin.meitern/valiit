package it.vali;

public class CarnivoreAnimal extends WildAnimal {
    public boolean isPackAnimal() {
        return packAnimal;
    }

    public void setPackAnimal(boolean packAnimal) {
        this.packAnimal = packAnimal;
    }

    private boolean packAnimal;

    public void killedOtherAnimal() {
        System.out.println("Sõin teise looma ära");
    }
}
