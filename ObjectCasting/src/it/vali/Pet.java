package it.vali;

public class Pet extends DomesticAnimal {
    public boolean isLivesOutside() {
        return livesOutside;
    }

    public void setLivesOutside(boolean livesOutside) {
        this.livesOutside = livesOutside;
    }

    private boolean livesOutside;

    public void pet() {
        System.out.println("Hullult mõnus on");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Elab õues: %s%n", livesOutside);
    }
}
