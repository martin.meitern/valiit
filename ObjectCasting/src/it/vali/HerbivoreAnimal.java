package it.vali;

public class HerbivoreAnimal extends WildAnimal {
    public boolean isHasHorns() {
        return hasHorns;
    }

    public void setHasHorns(boolean hasHorns) {
        this.hasHorns = hasHorns;
    }

    private boolean hasHorns;

    public void eatPlants() {
        System.out.println("Sõin just mingi taime ära");
    }
}
