package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int a = 100;
        short b = (short) a;

//        Iga kassi võib võtta kui looma.
//        implicit casting
        Animal animal = new Cat();

//        Iga loom ei ole kass
//        explicit casting
        Cat cat = (Cat)animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        dog.setName("Naki");
        CarnivoreAnimal lion = new CarnivoreAnimal();
        HerbivoreAnimal moose = new HerbivoreAnimal();

        animals.add(dog);
        dog.setName("Muki");
        animals.add(lion);
        animals.add(new Pet());
        ((Pet)animals.get(animals.size() - 1)).setOwnerName("Juhan");
        animals.get(animals.size() - 1).setName("Jaan");
        animals.add(moose);

//        kutsu kõikide listis "animals" olevate loomade printinfo()

        for (Animal animalInList: animals) {
            animalInList.printInfo();
            System.out.println();
        }

        animals.get(animals.indexOf(dog)).setName("Peeter");
        dog.printInfo();


        Animal secondAnimal = new Dog();

        ((Dog)secondAnimal).setHasTail(true);

        secondAnimal.printInfo();


    }
}
