package it.vali;

public class DomesticAnimal extends Animal {
    public String getOwnerName() {
        if (ownerName == null) {
            return "puudub";
        }
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    private String ownerName;

    public void isGivenFood() {
        System.out.println("Aitäh toidu eest, pai peremees");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Omaniku nimi: %s%n", getOwnerName());
    }
}
