package it.vali;

import java.util.Scanner;

public class Main {

 //        Meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust
//        Kui koodis on korduvaid koodijuppe, võiks nende kohta teha eraldi meetodi
    public static void main(String[] args) {
        printHello(2);
        System.out.print("Sisesta tekst, mida tahad välja printida: \n");
        Scanner scanner = new Scanner(System.in);
        String enterText = scanner.nextLine();
        System.out.print("Mitu korda tahad seda teksti välja printida?\n");
        int howManyTimes = Integer.parseInt(scanner.nextLine());
        System.out.print("Kas tahad, et tekst oleks suurte tähtedega??\n");
        String capitalize = scanner.nextLine();
        boolean toUpperCase = false;
        if (capitalize.equals("jah")) {
            toUpperCase = true;
        }
        printText(enterText, howManyTimes, toUpperCase);

//      Method OVRLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite kombinatsiooniga
    }
//    lisame meetodi, mis prindib ekraanile "Hello"
    private static void printHello() {
        System.out.println("Hello");
    }
//    Lisame parameetri, mis prindib "Hello" ette antud arv kordi
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.print("Hello\n");
        }
    }
    static void printText(String enterText, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUpperCase) {
                System.out.println(enterText.toUpperCase());
            }
            else {
                System.out.println(enterText);
            }
        }
    }
    static void printText(int year, String text) {
        System.out.printf("%d: %s%n", year, text);
    }
}
