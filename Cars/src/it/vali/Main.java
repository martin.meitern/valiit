package it.vali;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car();
        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();
        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car();

        Car opel = new Car("Opel", "Vectra", 1999, 205);
        opel.startEngine();
        opel.accelerate(205);

        Person person = new Person("Martin", "Meitern", Gender.MALE, 28);
        Person personTwo = new Person("Jaan", "Imelik", Gender.MALE, 100);
        Person personThree = new Person("Joosep", "Toots", Gender.MALE, 100);
        Person personFour = new Person("Kristjan", "Lible", Gender.MALE, 100);
        Person personFive = new Person("Arno", "Tali", Gender.MALE, 100);

        opel.slowDown(50);
        opel.slowDown(60);
        opel.park();

        Car audi = new Car();
        audi.setMake("Audi");
        audi.setModel("100");
        audi.setDriver(person);
        opel.setDriver(person);
        opel.setOwner(personTwo);

        System.out.println(audi.getDriver().getFirstName() + audi.getDriver().getLastName() + audi.getDriver().getAge());
        System.out.printf("Auto %s %s juht %s %s on %d aasta vanune%n",
                opel.getMake(), opel.getModel(), opel.getDriver().getFirstName(),
                opel.getDriver().getLastName(),  opel.getDriver().getAge());

        System.out.printf("Auto %s %s omanik %s %s on %d aasta vanune%n",
                opel.getMake(), opel.getModel(), opel.getOwner().getFirstName(),
                opel.getOwner().getLastName(),  opel.getOwner().getAge());
        System.out.printf("Auto %s %s juht on %s aasta vanune%n", audi.getMake(), audi.getModel(), audi.getDriver().getAge());

        opel.setNumberOfPassengers(4);

        opel.addPassenger(person);
        opel.addPassenger(personTwo);
        opel.addPassenger(personThree);
        opel.addPassenger(person);

        opel.showPassengers();

        opel.removePassenger(person);

        opel.showPassengers();

    }
}
