package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

enum Fuel {
    GAS, PETROL, DIESEL, HYBRID, ELECTRIC
}

public class Car {
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    private int numberOfPassengers;
    private List<Person> passengers = new ArrayList<Person>();

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    private Person driver;

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    private Person owner;

//    Konstruktor (constructor)
//    on eriline meetod, mis käivitatakse klassist objekti loomisel
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;
    }

    public Car(Person driver, Person owner) {
        this.driver = driver;
        this.owner = owner;
    }

//    Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel, boolean isUsed, Person driver, Person owner) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.driver = driver;
        this.owner = owner;
    }

    public void startEngine() {
        if (!isEngineRunning) {
            System.out.println("Mootor käivitus");
            isEngineRunning = true;
        }
        else {
            System.out.println("Mootor juba töötab");
        }
    }
    public void stopEngine() {
        if (isEngineRunning) {
            System.out.println("Mootor seiskus");
            isEngineRunning = false;
        }
        else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        }
        else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);
        }
        else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olle negatiivne");
        }
        else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        }
        else {
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
            speed = targetSpeed;
        }
    }
//    aeglustamise meetod
//    park meetod (kutsu välja juba olemasolevad meetodid)
//    Lisa autole parameetrid driver (tüübist Person) ja owner (tüübist Person) ja nendele get ja set meetodid
//    Loo mõni auto objekt, kellel on määratud kasutaja ja omanik
//    Prindi välja auto omaniku vanus

    public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        }
        else if (targetSpeed < 0) {
            System.out.println("Autot kiirus ei saa olla negatiivne");
        }
        else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustada suuremale kiirusele");
        }
        else {
            System.out.printf("Auto aeglustas kiiruseni %d%n", targetSpeed);
            speed = targetSpeed;
        }
    }

    public void park() {
        slowDown(0);
        stopEngine();
    }

    public void driverInfo() {
        System.out.printf("Auto %s %s juht %s %s on %d aasta vanune%n",
                make, model, driver.getFirstName(),
                driver.getLastName(), driver.getAge());
    }

//    lisa autole võimalus hoida max reisijaid
//    lisa meetodid reisijate lisamiseks ja eemaldamiseks
//    kontrolli, et ei lisaks rohkem reisijaid, kui mahub

    public void addPassenger(Person passenger) {
        if (numberOfPassengers > passengers.size()) {
            if (passengers.contains(passenger)) {
                System.out.printf("Autos juba on reisija %s %s%n", passenger.getFirstName(), passenger.getLastName());
                return;
            }
            System.out.printf("Autosse lisati reisija %s %s%n", passenger.getFirstName(), passenger.getLastName());
            passengers.add(passenger);
        }
        else {
            System.out.println("Auto on juba täis");
        }

    }

    public void removePassenger(Person passenger) {
        if (passengers.indexOf(passenger) != -1) {
            System.out.printf("Autost võeti ära reisija %s %s%n", passenger.getFirstName(), passenger.getLastName());
            passengers.remove(passenger);
        }
        else {
            System.out.println("Autos sellist reisijat pole");
        }
    }

    public void showPassengers() {
        System.out.println("Autos on järgmised reisijad:");

//        for-each tsükkel
//        Iga elemendi kohta listis "passengers" tekita objekt "passenger"
//        1. kordus Person passenger on esimene reisija
//        2. kordus Person passenger on teine reisija
        for (Person passenger : passengers) {
            System.out.println(passenger.getFirstName() + " " + passenger.getLastName());
        }
    }
}
