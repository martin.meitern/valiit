package it.vali;

enum Gender {
    FEMALE,
    MALE,
    NOTSPECIFIED
}

public class Person {
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }



    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;

//    Kui klassil ei ole defineeritud konstuktorit,
//    siis tehakse nähtamatu parameetrita konstuktor, mille sisu on tühi.
//    Kui klassile ise lisad amingi konstuktor, siis see nähtamatu parameetrita konstruktor kustutatakse
//    Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga
//    (ilma parameetriteta konstruktorit ei saa enam teha)

    public Person() {

    }

    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }
}
