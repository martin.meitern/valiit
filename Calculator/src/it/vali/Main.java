package it.vali;

import java.lang.reflect.Array;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        int sum = sum(4, 5);
//        System.out.printf("Arvude 4 ja 5 summa on %d%n", sum);
//        System.out.println("Arvude 5 ja 2 jagatis on " + divide(5, 2));
//
//        int[] numbers = new int[3];
//        numbers[0] = 1;
//        numbers[1] = 2;
//        numbers[2] = -2;
//        sum = sum(numbers);
//        System.out.println(sum);
//
//        numbers = new int[] {2, 4, 5, 6};
//        sum = sum(new int[] {1, 3, 4, 5});
//        System.out.println(sum);
//
//        printNumbers(reverseNumbers(numbers));
//
//        int[] reversed = reverseNumbers(new int[] {1, 2, 3, 4, 5});
//       printNumbers(reversed);
//
//       System.out.println(sum(reverseNumbers(new int[] {1, 3})));
//
//       printNumbers(stringToIntArray((new String[] {"2", "3", "4", "5"})));
        System.out.printf("Sõnade massiiv liidetuna sõnadeks on: %s%n",
                 arrayToText(new String[] {"Tere", "minu", "uus", "vihik"}));
        arrayToTextDelimiter(new String[] {"Tere", "minu", "uus", "vihik"}, ", ");
        System.out.println(average(new int[] {2, 3, 4, 5}));
        System.out.println(percentage(60, 60));
        System.out.println(circumference(4.0));

    }
//    Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    static int subtract(int a, int b){
        return a - b;
    }

    static int multiply(int a, int b) {
        return a * b;
    }

    static double divide(int a, int b) {
        return (double)a / b;
    }
    static int sum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
    //Meetod, mis võtab argumendiks täisarvude massiivi, pöörab selle tagurpidi ja tagastab selle.
    static int[] reverseNumbers(int[] numbers) {
        int[] reversedNumbers = new int[numbers.length];
        for (int i = 0, j = numbers.length - 1; i < numbers.length; i++, j--) {
            reversedNumbers[i] = numbers[j];
        }
        return reversedNumbers;
    }
    /// meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }


    //Meetod, mis võtab parameetriks numbritest koosneva stringi massiivi ja tagastab numbrite massiivi.
    static int[] stringToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    //Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik
    //
    static String arrayToText(String[] arrayText) {
        String newSentence = String.join(" ", arrayText);
        return newSentence;
    }
    //Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause ning teine parameeter tähistab sõnade eraldajat
    //

    static void arrayToTextDelimiter(String[] arrayText, String delimiter) {
        String newSentence = String.join(delimiter, arrayText);
        System.out.println(newSentence);
    }
    //Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle
    //
    static double average(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum+=numbers[i];
        }
        return (double)sum / numbers.length;
    }
    //Meetod, mis arvutab mitu % moodustab esimene arv teisest arvust
    //

    static double percentage(int a, int b) {
        return 100 * a / (double)b;
    }
    //Meetod, mis leiab ringi ümbermõõdu raadiuse järgi

    static double circumference(double radius) {
        return Math.PI * 2 * radius;
    }


}
