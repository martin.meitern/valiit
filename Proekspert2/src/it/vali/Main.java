package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Tere supervaisor");
        List<String> linesFromFile = readFromFile("copy.log");
        List<LogEvent> logEvents = new ArrayList<LogEvent>();

        for (int i = 0; i < linesFromFile.size(); i++) {
            logEvents.add(parseLine(linesFromFile.get(i)));
        }

//          TEST
//        for (int i = 0; i < logEvents.size(); i++) {
//            LogEvent test = logEvents.get(i);
//            System.out.println("Event:" + test.getResourceName());
//        }

        Map<String, List<Double>> resourceTime = new HashMap<String, List<Double>>();
        for (int i = 0; i < logEvents.size(); i++) {
            LogEvent le = logEvents.get(i);
            if (resourceTime.containsKey(le.getResourceName())) {
                resourceTime.put(le.getResourceName(), resourceTime + );
            }
            else {
                resourceTime.put(le.getResourceName(), le.getDuration());
            }


        }

//        TEST
        for (String name: resourceTime.keySet()){
            String key = name.toString();
            String value = resourceTime.get(name).toString();
            System.out.println(key + " " + value);
        }

    }

    static List<String> readFromFile(String filename) {
        List<String> lines = new ArrayList<String>();
        try {
            FileReader fileReader = new FileReader(filename);

            BufferedReader bufferedReader = new BufferedReader((fileReader));
            String line = bufferedReader.readLine();

            do {
                lines.add(line);
                line = bufferedReader.readLine();
            }
            while (line != null);

            bufferedReader.close();
            fileReader.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }


    static LogEvent parseLine(String line) {
        System.out.println(line);
        String lineRegex = "^(\\S+\\s\\S+)\\s(\\S+)\\s(\\S+)\\s(\\S+)\\s(.*)in\\s([0-9]+)$";
        Matcher matcher = Pattern.compile(lineRegex).matcher(line);
        LogEvent logEvent = new LogEvent();
        if (matcher.matches()) {
            logEvent.setTimestamp(matcher.group(1));
            logEvent.setDuration(Integer.parseInt(matcher.group(6)));
            logEvent.setResourceName(matcher.group(4));
        }
        return logEvent;
    }

}