package it.vali;

import java.util.List;

public class LogEvent {
    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public List<Double> getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private String resourceName;
    private int duration;
    private String timestamp;

//    public LogEvent(String resourceName, int duration, String timestamp) {
//        this.resourceName = resourceName;
//        this.duration = duration;
//        this.timestamp = timestamp;
//    }
}
