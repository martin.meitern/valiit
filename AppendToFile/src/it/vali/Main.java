package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter("output.txt", true);
            fileWriter.append("Tere\r\n");

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//    Koosta täisarvude massiiv 10 arvust ning seejärel kirjuta faili kõik suuremad arvud, kui 2
        int[] tenNumbers = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        try {
            FileWriter fileWriter = new FileWriter("numbersbiggerthantwo.txt", true);
            for (int i = 0; i < tenNumbers.length; i++) {
                if (tenNumbers[i] > 2) {
                    fileWriter.write(String.valueOf(tenNumbers[i]) + System.lineSeparator());
            }
        } fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//    Küsi kasutajalt kaks arvu.
//    Küsi seni, kuni mõlemad on korrektsed(on nubmrid) ning nende summa ei ole paarisarv

      Scanner scanner = new Scanner(System.in);
        int a = 0;
        int b = 0;
        do {
            boolean correctNumber = false;
            System.out.println("Sisesta 2 numbrit, mille summa ei ole paarisarv");
            do {
                System.out.println("Esimene number");
                try {
                    a = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Sa ei sisetanud numbrit");
                }
            } while (!correctNumber);
            do {
                correctNumber = false;
                System.out.println("Teine number");
                try {
                    b = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Sa ei sisetanud numbrit");
                }
            } while (!correctNumber);
        } while (((a + b) % 2) == 0);
        System.out.printf("Arvude %d ja %d summa on %d%n", a, b, sum(a, b));

      System.out.println("Mitu numbrit sa tahad sisestada?");
      int howManyNumbers = Integer.parseInt(scanner.nextLine());
      int[] numbers = new int[howManyNumbers];
        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("Sisesta %d. number%n", i+1);
            int number = Integer.parseInt(scanner.nextLine());
            numbers[i] = number;
        }
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        try {
            FileWriter fileWriter = new FileWriter("array.txt", true);
            fileWriter.append(sum + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    static int sum(int a, int b){
        return a + b;
    }




//    Küsi kasutajalt mitu arvu ta tahab sisestada
//    seejärel küsi kasutajalt arvud ühekaupa
//    ning kirjuta nende arvude summa faili. Nii et see lisataks alati juurde
}
