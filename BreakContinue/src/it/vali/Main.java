package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // Küsime kasutajalt pin-koodi. Kui see on õige, siis ütleme "Tore".
        // Muul juhul küsime uuesti.
        // kokku küsime 3 korda.

        String realPin = "1234";
        Scanner scanner = new Scanner(System.in);
        //Break hüppab tsüklist välja
//        for (int i = 0; i < 3; i++) {
//            System.out.println("Sisesta oma pin-kood");
//            String userPin = scanner.nextLine();
//            if (userPin.equals(realPin)) {
//                System.out.println("Tore. Sisestasid õige pin-koodi.");
//                break;
//            }
//        }
//        int retriesLeft = 3;
//        String userPin;
//        do {
//            System.out.println("Sisesta oma pin-kood");
//            retriesLeft--;
//        }
//        while (!scanner.nextLine().equals(realPin) && retriesLeft > 0);
//
//        if () {
//            System.out.println("Panid 3 korda pin-koodi valesti");
//        }
//        else {
//            System.out.println("Tore. Sisestasid õige pin-koodi.");
//        }
        // Continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <= 60; i++) {
            if (i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }


    }
}
