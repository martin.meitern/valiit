package it.vali;

public class Main {

    public static void main(String[] args) {
	    Monitor firstMonitor = new Monitor();
	    Monitor secondMonitor = new Monitor();
	    Monitor thirdMonitor = new Monitor();

	    firstMonitor.manufacturer = "Philips";
	    firstMonitor.color = Color.WHITE;
	    firstMonitor.diagonal = 27;
	    firstMonitor.screenType = ScreenType.AMOLED;

	    secondMonitor.manufacturer = "Samsung";
	    secondMonitor.color = Color.BLACK;
	    secondMonitor.diagonal = 24;
	    secondMonitor.screenType = ScreenType.LCD;

	    thirdMonitor.manufacturer = "Apple";
	    thirdMonitor.color = Color.GREY;
	    thirdMonitor.diagonal = 32;
	    thirdMonitor.screenType = ScreenType.TFT;

		System.out.println(firstMonitor.manufacturer);
		System.out.println(secondMonitor.screenType);

		firstMonitor.color = Color.BLACK;
		System.out.println(firstMonitor.color);
		System.out.println("Prindi välja kõigi monitoride tootja, mille diagonaal on suurem, kui 25 tolli");

		Monitor[] monitors = new Monitor[4];
		monitors[0] = firstMonitor;
		monitors[1] = secondMonitor;
		monitors[2] = thirdMonitor;

		monitors[3] = new Monitor();
		monitors[3].manufacturer = "Sony";
		monitors[3].color = Color.BLACK;
		monitors[3].diagonal = 21;
		monitors[3].screenType = ScreenType.LCD;

		for (int i = 0; i < monitors.length; i++) {
			if (monitors[i].diagonal > 25) {
				System.out.println(monitors[i].manufacturer);
			}
		}
//		Leia kõige suurema monitori värv
		System.out.println("Leia kõige suurema monitori värv");
		Monitor maxSizeMonitor = monitors[0];
		for (int i = 0; i < monitors.length; i++) {
			if (monitors[i].diagonal > maxSizeMonitor.diagonal) {
				maxSizeMonitor = monitors[i];
			}
		}
		maxSizeMonitor.printInfo();
		System.out.printf("Monitori diagonaal sentimeetrites on %.2f%n", maxSizeMonitor.diagonalToCm());
    }
}
