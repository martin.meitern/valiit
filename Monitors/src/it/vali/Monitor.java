package it.vali;

//enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
//Tegelikult salvestatakse enum alati int-na
enum Color {
    BLACK,
    WHITE,
    GREY
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

public class Monitor {
    String manufacturer;
    double diagonal; //tollides "
    Color color;
    ScreenType screenType;

    void printInfo() {

        System.out.println("Monitors info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", screenType);

    }

//    tee meetod, mis tagastab ekraani diagonaali sentimeetrites
    double diagonalToCm() {
        return diagonal * 2.54;
    }
}
