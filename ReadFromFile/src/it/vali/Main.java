package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader((fileReader));

//            readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

//          kui readline avastab, et järgmist rida tegelikult ei ole, siis tagastab ta null
//            while (line != null) {
//                System.out.println(line);
//                line = bufferedReader.readLine();
//            }

            do {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
            while (line != null);

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
