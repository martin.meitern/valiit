package it.vali;

public class Main {

    public static void main(String[] args) {
        // muutujat deklareerida ja talle väärtust anda saab ka kahe sammuga
        String word;
        word = "Kala";

        int number;
        number = 3;
        int secondNumber = -7;
        System.out.println(number);
        System.out.println(secondNumber);

        System.out.println(number + secondNumber);

        //Arvude 3 ja -7 korrutis on -21
        int multiplication = number * secondNumber;

        /*%n tekitab platvormispetsiifilise reavahetuse (Windows \r\n ja Linux/Mac \n)
        selle asemel saab kasutada ka System.liteSeparator()*/
        System.out.printf("Arvude %d ja %d korrutis on %d.%n",
                number, secondNumber, number * secondNumber);
        System.out.printf("Arvude %d ja %d korrutis on %d.%n",
                number, secondNumber, multiplication);
        int a = 3;
        int b = 9;
        // String + number teisendatakse number stringiks ja liidetakse kui liitsõna
        System.out.println("Arvude summa on " + (a + b));

        /* kahe täisarvu jagamisel on tulemus jagatise täisosa ehk kõike peale
        koma süüakse ära
         */
        System.out.printf("Arvude %d ja %d jagatis on %d%n",
        a, b, a/b);
        System.out.printf("Arvude %d ja %d jagatis on %d%n",
                b, a, b/a);

       int maxInt = 2147483647;
       int c = maxInt + 1;
       int d = maxInt + 2;
       System.out.println(c);
       System.out.println(d);

       int minInt = -2147483648;
       int e = minInt -1;
       System.out.println(e);

       short f = 32199;

       /* long väärtust andes peab max numbrile L tähe lõppu lisama
       Lühema numbri puhul ei pea L tähte lisama
        */
       long g = 99999999L;
    }
}
