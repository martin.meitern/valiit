package it.vali;

import java.util.List;

public class Country {
    private int population;
    private String name;
    private List<String> languages;

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        String languagesString = String.join(", ", languages);
        return ("Riigis " + name + " elab " + population + " inimest ja seal räägitakse järgmisi keeli: " + languagesString);
    }
}
