package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        1. ülesanne
        System.out.println("Esimene ülesanne");

	    double pi = Math.PI;
	    System.out.println(pi*2);

//	    2. ülesanne
        System.out.println();
        System.out.println("Teine ülesanne");

        System.out.println(areNumbersEqual(4, 4));

//      3. ülesanne
        System.out.println();
        System.out.println("Kolmas ülesanne");

        String[] words = new String[]{"123", "1", "teretere", "tereteretere"};
        int[] numbersInWords = new int[words.length];
        numbersInWords = stringToIntArray(words);
        for (int i = 0; i < numbersInWords.length; i++) {
            System.out.println(numbersInWords[i]);
        }

//      4. ülesanne
        System.out.println();
        System.out.println("Neljas ülesanne");

        System.out.println(findCentury(0));
        System.out.println(findCentury(1));
        System.out.println(findCentury(128));
        System.out.println(findCentury(598));
        System.out.println(findCentury(1624));
        System.out.println(findCentury(1827));
        System.out.println(findCentury(1996));
        System.out.println(findCentury(2017));


//      5. ülesanne
        System.out.println();
        System.out.println("Viies ülesanne");

        Country country = new Country();
        country.setName("Šveits");
        country.setPopulation(8508898);
        List<String> languages = new ArrayList<>();
        languages.add("saksa");
        languages.add("prantsuse");
        languages.add("itaalia");
        languages.add("romanši");
        country.setLanguages(languages);

        System.out.println(country);



    }

//    2. ülesanne
    static boolean areNumbersEqual(int a, int b) {
        return a == b;
    }

//    3. ülesanne
    static int[] stringToIntArray(String[] array) {
        int[] intArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            intArray[i] = lettersInString(array[i]);
        }
        return intArray;
    }

    static int lettersInString(String word) {
        int counter = 0;
        for (int i = 0; i < word.length(); i++) {
            counter++;
        }
        return counter;
    }

//    4. ülesanne
    static byte findCentury(int year) {
        byte century = -1;
        if (year >= 1 && year <= 2018) {
            century = (byte) (year / 100 + 1);
        }
        return century;
    }
}
