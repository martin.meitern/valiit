package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    /*
	    Kõigepealt küsitakse külastaja nime.
	    Kui nimi on listis, siis öeldakse "Tere tulamast [nimi]"
	    ja küsitakse külastaja vanust.
	    Kui kasutaja on alaealine, siis teavitatakse teda "Sorry, sisse ei saa".
	    Muul juhul öeldakse "Tere tulemast klubisse".
	     */

	    /* Kui kasutaja ei olnud listis, siis küsitakse kasutajalt ka tema perekonnanime.
	    Kui perekonnanimi on listis, siis öeldakse "Tere tulemast [eesnimi + perekonnanimi]
	    Muul juhul öeldakse "Ma ei tunne sind".
	     */

	    String listFirstName = "Martin";
	    String listLastName = "Meitern";
	    System.out.println("Mis su nimi on?");
	    Scanner scanner = new Scanner(System.in);
	    String name = scanner.nextLine();

	    if (name.toLowerCase().equals(listFirstName.toLowerCase())) {
	        System.out.printf("Tere tulemast, %s%n", listFirstName);
	        System.out.println("Kui vana sa oled?");
	        int age = Integer.parseInt(scanner.nextLine());
	        if (age < 18) {
	        	System.out.println("Sorry, sisse ei saa");
			}
	        else {
	        	System.out.println("Tere tulemast klubisse!");
			}
        }
	    else {
			System.out.println("Mis su perekonnanimi on?");
			String lastName = scanner.nextLine();
			if (lastName.toUpperCase().equals(listLastName.toUpperCase())) {
				System.out.printf("Tere tulemast, %s %s%n", listFirstName, listLastName);
				System.out.println("Kui vana sa oled?");
				int age = Integer.parseInt(scanner.nextLine());
				if (age < 18) {
					System.out.println("Sorry, sisse ei saa");
				}
				else {
					System.out.println("Tere tulemast klubisse!");
				}
			}
			else {
				System.out.println("Sorry, ma ei tunne sind");
			}
		}
    }
}
