﻿-- See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). selles veerus ja reas saab olema tekst "Hello World".
SELECT 'Hello World';

SELECT 3;
SELECT true;
SELECT 3.32;
SELECT 3+4;
SELECT 3/4.0;

-- 'raud' + 'tee' ei tööta PostgreSQL'is. Näiteks MSSql'is töötab plussiga stringide liitmine
SELECT 'raud' + 'tee';

-- õige viis sõnade liitmiseks. Standard CONCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT ('raud', 'tee', 'jaam', 1, 88, 9);

-- Erinevate veergude saamiseks tuleb väärtustele koma vahele panna
SELECT 'Peeter', 'Paat', 23, 74.45, true;

-- AS märksõnaga saab anda antud veerule nime
SELECT 
'Peeter' AS eesnimi, 
'Paat' AS perekonnanimi, 
23 AS vanus, 
74.45 AS kaal, 
true AS blond;

-- Kellaaja pärimine
SELECT NOW() AS aeg;
-- mingi konkreetse osa saamiseks, näiteks aasta või kuu, on olemas eraldi meetod
SELECT date_part('years', NOW());
SELECT date_part('month', NOW());
SELECT date_part('day', NOW());

-- kuupäeva formaadi teisendamine 
SELECT to_char(Now(), 'HH24:mi:ss DD.MM.YYYY');

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now() + interval '2 weeks'
SELECT now() + interval '2 centuries 3 year 2 months 1 weeks 3 days 4 seconds'

-- Kui tahad ise kuupäeva määrata
SELECT date_part('month', TIMESTAMP '2019-01-01');
SELECT date_part('minutes', TIME '10:10');


CREATE TABLE student(
	id serial PRIMARY KEY,
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema 
	-- PRIMARY KEY tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- EI TOHI tühi väli olla
	last_name varchar(64) NOT NULL,
	height int NULL, --TOHIB tüli väli olla
	weight numeric(5, 2) NULL,
	birthday date NULL
);

-- Tabelist kõikide ridade kõikide veergude küsimine
-- * tähendab, et anna kõik veerud
SELECT * FROM student;

SELECT 
	first_name AS eesnimi,
	last_name AS perekonnanimi,
	date_part('year', now()) - date_part('year', birthday) AS vanus
FROM 
	student;
	
-- Kindla muutuja järgi otsimiseks/filtreerimiseks tuleb kasutada märksõna WHERE
SELECT 
	* 
FROM 
	student
WHERE
	height = 180
	
-- Mitme kriteeriumi järgi otsimiseks/filtreerimiseks tuleb kasutada märksõnu OR või AND
SELECT 
	first_name,
	last_name
FROM 
	student
WHERE
	(first_name = 'Peeter' AND last_name = 'Tamm')
	OR (first_name = 'Mari' AND last_name = 'Maasikas');
	
-- anna inimesed, kelle pikkus jääb 170-180 cm vahele
SELECT 
	*
FROM 
	student
WHERE
	height >= 170
	AND height <= 180;
	
-- Anna mulle inimesed, kes on pikemad kui 170cm või lühemad kui 150cm
SELECT 
	*
FROM 
	student
WHERE
	height > 170
	OR height < 150;
	
-- Anna mulle inimeste nimed ja pikkus, kellel on sünnipäev jaanuaris
SELECT 
	first_name,
	height,
	birthday
FROM 
	student
WHERE
	date_part('month', birthday) = 1;

-- Anna mulle inimesed, kelle middle_name on NULL
SELECT 
	*
FROM 
	student
WHERE
	middle_name IS NULL;
	
-- Anna mulle inimesed, kelle middle_name ei ole NULL
SELECT 
	*
FROM 
	student
WHERE
	middle_name IS NOT NULL;
	
-- Otsi inimesed, kelle pikkus ei ole 180cm !=
SELECT 
	*
FROM 
	student
WHERE
	height != 180;
	
-- VÕI <>
SELECT 
	*
FROM 
	student
WHERE
	height <> 180;
	
-- Otsi inimesed, kelle pikkus on kas 169 VÕI 202
SELECT 
	*
FROM 
	student
WHERE
	height IN (169, 202);

-- Otsi mulle inimesed, kelle eesnimi on Peeter, Mari või Kalle
SELECT 
	*
FROM 
	student
WHERE
	first_name IN ('Peeter', 'Mari', 'Kalle')
	
-- Otsi mulle inimesed, kelle eesnimi ei ole Peeter, Mari või Kalle
SELECT 
	*
FROM 
	student
WHERE
	first_name NOT IN ('Peeter', 'Mari', 'Kalle')
	
-- Otsi mulle inimesed, kelle sünnikuupäev on kuu esimene, neljas või seitsmes päev
SELECT 
	*
FROM 
	student
WHERE
	date_part('day', birthday) IN (1, 4, 7);
	
-- Kui mõni väärtus on NULL, siis WHERE võrdlused seda ei kuva
SELECT 
	*
FROM 
	student
WHERE
	height <> 202; -- EI TAGASTA NULL'i
	
-- Otsi mulle õpilased pikkuse järjekorras lühemast pikemaks
-- Kui pikkused on võrdsed, järjesta kaalu järgi. ORDER BY
SELECT 
	*
FROM 
	student
ORDER BY
	first_name,
	last_name
	
-- Vastupidise järjekorra saamiseks lisandub DESC (ASC on vaikeväärtus)
SELECT 
	*
FROM 
	student
ORDER BY
	first_name DESC,
	last_name DESC,

-- Otsi vanuse järjekorras vanemast nooremaks õpilased, kelle pikkus jääb 140 ja 181 vahele
SELECT 
	*
FROM 
	student
WHERE
	height > 140 AND height < 181
ORDER BY
	birthday
	
--  Mingi kindla algustähega inimeste otsimiseks
SELECT 
	*
FROM 
	student
WHERE
	first_name LIKE 'P%'
	
	--Tabelisse uute kirjete lisamine
INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Tiina', 'Tihane', 166, 66.54, '1992-11-22', NULL),
	('Maria', 'Tihane', 169, 64.01, '1988-11-22', NULL),
	('Tiit', 'Tihane', 176, 80.86, '1981-11-22', NULL);
	
-- Tabelis kirje muutmisega UPDATE lausega peab olema ettevaatlik. Alati peab kasutama WHERE'i lause lõpus
UPDATE
	student
SET
	height = 172
WHERE
	id = 4
	
-- Muuda kõigi õpilaste pikkust 1 cm võrra suuremaks
UPDATE
	student
SET
	height = height + 1

-- Suurenda hiljem, kui 1999 sündinud õpilastel ühe päeva võrra
UPDATE
	student
SET
	birthday = birthday + interval '1 day'
WHERE
	date_part('year', birthday) > 1999
	
--Kustutamisega tuleb samuti lisada WHERE lause
DELETE FROM
	student
WHERE
	id = 5
	
-- CRUD operations
-- create, read, update, delete

-- Loo tabel 'loan', millel on väljad: 
--amount(reaalarv), start_date, due_date, student_id
CREATE TABLE loan (
	id serial PRIMARY KEY,
	amount numeric(11, 2) NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa laenud
INSERT INTO	loan
	(amount, start_date, end_date, student_id)
VALUES
	(1000, '12-02-2018', '02-12-2019', 1),
	(5000, '02-01-2018', '30-01-2019', 8),
	(500, NOW(), '12-12-2019', 10),
	(4, '06-01-2017', '02-12-2022', 11),
	(2345, NOW(), '01-05-2050', 1),
	(100, '02-02-2015', '02-12-2025', 11);
	
-- Anna mulle kõik õpilased koos oma laenudega
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read, kus on võrdsed student.id = load.student_id
-- ehk need read, kus tabelite vahel on seos. Ülejäänud ridasid ignoreeritakse
SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
INNER JOIN --INNER JOIN on vaikimisi JOIN. INNER võib eest ära jätta
	loan
	ON student.id = loan.student_id
	
-- Anna kõik õpilased, kellel on laen vähemalt 400€
SELECT
	student.first_name AS eesnimi,
	student.last_name AS perekonnanimi,
	loan.amount AS laenu_summa
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 400
ORDER BY
	student.last_name,
	loan.amount DESC
	
-- Loo uus tabel loan_type väljadega name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

-- Uue välja lisamine olemasolevasse tabelisse
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- Tabeli kustutamine
DROP TABLE student;

-- Lisame mõned laenutüübid
INSERT INTO loan_type
	(name, description)
VALUES
	('Õppelaen', 'Õppelaen on mõeldud tudengitele'),
	('Kodulaen', 'Selle eest saad maja osta'),
	('SMS laen', 'Seda pakuvad vastikud värdjad'),
	('Remondilaen', 'Tee kodu korda');
	
-- Kolme tabeli INNER JOIN
SELECT
	s.first_name,
	s.last_name,
	l.amount,
	lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- INNER JOIN puhul ei ole ühendamiste järjekord oluline
SELECT
	s.first_name,
	s.last_name,
	l.amount,
	lt.name
FROM
	loan_type AS lt
JOIN
	LOAN as L
	ON lt.id = l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id
	
-- LEFT JOIN puhul võetakse JOIN'i esimesest (vasakpoolsest) tabelist kõik read
-- ning teises (parempoolses) tabelis näidatakse puuduvates kohtades NULL
SELECT
	s.first_name,
	s.last_name,
	l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id

-- LEFT JOIN puhul on järjekord oluline
SELECT
	s.first_name,
	s.last_name,
	l.amount,
	lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- CROSS JOIN'iga saab kõik võimalikud kahe tabeli kombinatsioonid
SELECT
	s.first_name,
	st.first_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE
	s != st
	
-- FULL OUTER JOIN näitab mõlema tabeli kõiki ridu
SELECT
	s.first_name,
	s.last_name,
	l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu ja kelle laenu kogus on üle 100€. Tulemused järjesta vanuse järgi väiksemast suuremaks
SELECT
	s.first_name,
	s.last_name,
	l.amount,
	lt.name
FROM
	loan AS l
JOIN
	loan_type as lt
	ON lt.id = l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id
	
-- Aggregate functions
-- Agregaatfunktsiooni SELECT'is välja kutsudes kaob võimalus samas SELECT lauses küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on 1 number.
--Seda ei saa näidata koos väljadega, mida võib olla mitu rida
-- Keskmise arvutamine. Jäetakse välja read, kus height = NULL
SELECT
	AVG(height)
FROM
	student
	
-- Kui palju võtsid õpilased keskmiselt laenu. COALESCE arvestab ka neid õpilasi, kes ei ole laenud võtnud (nende puhul amount = 0)
SELECT
-- 	student.first_name,
-- 	student.last_name,
	AVG(COALESCE (loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- ülejäänud agregaatfunktsioonid
SELECT
	ROUND(AVG(loan.amount), 0) AS "Kõikide õpilaste keskmine laenusumma",
	ROUND(AVG(COALESCE (loan.amount,0)), 0) AS "Laenuvõtjate keskmine laenusumma",
	MIN(loan.amount) AS "Väikseim laenusumma",
	MAX(loan.amount) AS "Suurim laenusumma",
	COUNT(loan.amount) AS "Laenude arv kokku",
	COUNT(*), -- kõikide ridade arv
	SUM(loan.amount) AS "Laenude summa kokku"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- Kasutades GROUP BY, jäävad SELECT päringu jaoks vaid need välja, mis on GROUP BY's välja toodud (s.first_name, s.last_name). Teisi välju saab ainult kasutada agregaatfunktsioonide sees.
SELECT
	s.first_name, 
	s.last_name,
	SUM(l.amount),
	COUNT(l.amount),
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, 
	s.last_name
ORDER BY
	SUM(l.amount) DESC
	
-- Anna laenude summad sünniaastate järgi
SELECT
	date_part('year', s.birthday),
	SUM(l.amount),
	COUNT(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	date_part('year', s.birthday)

-- Anna laenude summad laenu tüüpide järgi
SELECT
	lt.name,
	SUM(l.amount),
	COUNT(l.amount)
FROM
	loan AS l
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name
	
-- HAVING on nagu WHERE aga peale GROUP BY kasutamist. Filtreerimisel saab kasutada ainult neid välju, mis on GROUP BY's ja agregaatfunktsioone
SELECT
	date_part('year', s.birthday),
	SUM(l.amount),
	COUNT(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000
	
-- Tekita mingite õpilasele 2 sama tüüpi laenu. Anna laenuda summad grupeerituna õpilase ning laenu tüübi kaupa
SELECT
	s.first_name,
	s.last_name,
	lt.name,
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan as l
	ON l.student_id = s.id
JOIN
	loan_type as lt
	ON lt.id = l.loan_type_id
GROUP BY
	s.first_name,
	s.last_name,
	lt.name
	
-- Anna mitu laenu mingist tüübist on võetud ning mis on nende summad
SELECT
	lt.name,
	sum(l.amount),
	COUNT(l.amount)
FROM
	loan as l
RIGHT JOIN
	loan_type as lt
	ON lt.id = l.loan_type_id
GROUP BY
	lt.name

-- Mis aastal sündinud võtsid kõige suurema summa laenu?
SELECT
	date_part('year', s.birthday),
	SUM(l.amount),
	COUNT(l.amount)
FROM
	student as s
JOIN
	loan as l
	ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	SUM(l.amount) DESC
LIMIT 1

-- Anna õpilaste eesnime esitähe esinemiste statistika. Mitme õpilase eesnimi algab mingi kindla tähega
SELECT
	SUBSTRING(first_name, 1, 1),
	COUNT(SUBSTRING(first_name, 1, 1))
FROM
	student
GROUP BY
	SUBSTRING(first_name, 1, 1)
ORDER BY
	SUBSTRING(first_name, 1, 1)
	
SELECT FORMAT ('Hello %s', 'World')

-- Anna õpilased, kelle pikkus vastab keskmisele pikkusele
SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height), 0) FROM student)

-- Subquery, inner query, nested query
-- Anna õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi

SELECT
	first_name,
	last_name,
	(SELECT weight FROM student WHERE id = 8)
FROM
	student
WHERE
	first_name IN

(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height), 0) FROM student))
	
-- Anna kõik õpilased, kes õpivad matemaatikat	
SELECT
	first_name,
	last_name
FROM
	student AS st
JOIN
	student_subject AS ss
	ON ss.student_id = st.id
JOIN
	subject AS su
	ON su.id = ss.subject_id
WHERE
	su.name = 'matemaatika'
	
-- Anna kõik tunnid, kus õpib Mari
SELECT
	su.name
FROM
	student AS st
JOIN
	student_subject AS ss
	ON ss.student_id = st.id
JOIN
	subject AS su
	ON su.id = ss.subject_id
WHERE
	st.first_name = 'Mari'