package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";
//        Split tükeldab stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
//        | tähendab, et regulaaravalises "või"
        String[] words = sentence.split(" ja | |, ");
//      String[] words = sentence.split(" ", 3);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        String newSentence = String.join(" ", words);
        System.out.println(newSentence);

//        newSentence = String.join(", ", words);
//        System.out.println(newSentence);
//
//        newSentence = String.join(" ja ", words);
//        System.out.println(newSentence);
//
//        newSentence = String.join("\t", words);
//        System.out.println(newSentence);
////      Escaping
//        System.out.println("Juku \007 \bütles:\n\\n \"Mulle meeldib suvi.\"");
//        System.out.println("C:\\Program Files (x86)");

        System.out.println("Küsi kasutajalt rida numbreid, nii et ta paneb need numbrid kirja ühele reale, eraldades tühikuga. Seejärel liidab need kõik numbrit kokku ja prindib vastuse");
        System.out.println("Sisesta arvud, mida tahad omavahel liita, kasutades tühikut:");
        Scanner scanner = new Scanner(System.in);

        String numbersText = scanner.nextLine();

        String[] numbers = numbersText.split(" ");
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += Integer.parseInt(numbers[i]);
        }
        String newNumbers = String.join(", ", numbers);
        System.out.printf("Arvude %s summa on %d%n", newNumbers, sum);
    }
}
