package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    static String pin;

    public static void main(String[] args) {
        // Hoiame pin koodi failis pin.txt
        // ja kontojääki balance.txt
        pin = loadPin();
        int balance = loadBalance();
        Scanner scanner = new Scanner(System.in);
//        int triesLeft = 3;
//        System.out.println("Sisesta PIN-kood");

        if (!validatePin()) {
            return;
        }
        boolean wrongAnswer = false;
        int whichAction = 0;
        do {
            do {
                System.out.println("Vali toiming:");
                System.out.println("a) sularaha sissemakse");
                System.out.println("b) sularaha väljamakse");
                System.out.println("c) kontoväljavõte");
                System.out.println("d) muuda PIN-koodi");
                System.out.println("e) lõpeta ja tagasta kaart");
                String answer = scanner.nextLine();
                switch (answer) {
                    case "a":
                        whichAction = 1;
                        wrongAnswer = false;
                        break;
                    case "b":
                        whichAction = 2;
                        wrongAnswer = false;
                        break;
                    case "c":
                        whichAction = 3;
                        wrongAnswer = false;
                        break;
                    case "d":
                        whichAction = 4;
                        wrongAnswer = false;
                        break;
                    case "e":
                        whichAction = 5;
                        wrongAnswer = false;
                        break;
                    default:
                        System.out.println("Selline valik puudub");
                        wrongAnswer = true;
                        break;
                }
            } while (wrongAnswer);
            if (whichAction == 1) {
                System.out.println("Palun sisestage summa, mida soovite pangakontole kanda:");
                int amountInserted = Integer.parseInt(scanner.nextLine());
                balance += amountInserted;
                saveBalance(balance);
                saveAllBalances(balance);
                System.out.printf("Lisasite kontole %d ja pärast seda tehingut on teie pangakontol %d eurot%n", amountInserted, balance);

            }
            else if (whichAction == 2) {
                boolean wrongAnswerTwo = false;
                int amount = 0;
                do {
                    System.out.println("Vali summa, mis soovid välja võtta");
                    System.out.println("a) 5€");
                    System.out.println("b) 10€");
                    System.out.println("c) 20€");
                    System.out.println("d) muu summa");
                    String answer = scanner.nextLine();
                    switch (answer) {
                        case "a":
                            if (balance >= 5) {
                                balance-=5;
                                saveBalance(balance);
                                saveAllBalances(balance);
                                System.out.println("Võta palun oma raha");
                            }
                            else {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            wrongAnswerTwo = false;
                            break;
                        case "b":
                            if (balance >= 10) {
                                balance-=10;
                                saveBalance(balance);
                                saveAllBalances(balance);
                                System.out.println("Võta palun oma raha");
                            }
                            else {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            wrongAnswerTwo = false;
                            break;
                        case "c":
                            if (balance >= 20) {
                                balance-=20;
                                saveBalance(balance);
                                saveAllBalances(balance);
                                System.out.println("Võta palun oma raha");
                            }
                            else {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            wrongAnswerTwo = false;
                            break;
                        case "d":
                            System.out.println("Kui palju tahaksid välja võtta?");
                            amount = Integer.parseInt(scanner.nextLine());
                            if (balance >= amount) {
                                balance-=amount;
                                saveBalance(balance);
                                saveAllBalances(balance);
                                System.out.println("Võta palun oma raha.");
                            }
                            else {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            wrongAnswerTwo = false;
                            break;
                        default:
                            System.out.println("Selline valik puudub");
                            wrongAnswer = true;
                            break;
                    }
                } while (wrongAnswer);
            }
            else if (whichAction == 3) {
                loadAllBalance();
            }
            else if (whichAction == 4){
                System.out.println("Sisesta PIN-kood");
                String userPin = scanner.nextLine();
                String newPinSecond = null;
                String newPin = null;
                if (userPin.equals(loadPin())) {
                    do {
                        System.out.println("Sisesta uus PIN-kood");
                        newPin = scanner.nextLine();
                        System.out.println("Sisesta uuesti uus PIN-kood");
                        newPinSecond = scanner.nextLine();
                    }
                    while (!newPin.equals(newPinSecond));
                    savePin(newPin);
                }
                else {
                    System.out.println("Vale PIN");
                }
            }
        } while (whichAction!=5);
    }

    static void savePin(String pin) {
        // Toimub pin kirjutamine pin.txt faili
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    static String loadPin() {
        // Toimub pin välja lugemine pin.txt failist
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi laadimine ebaõnnestus");
        }
        return pin;
    }

    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }

    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }
    static String currentDateTimeToString() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return dateFormat.format(date);
    }
    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta PIN kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(pin)) {
                System.out.println("Tore! Õige pin kood");
                return true;
            }
        }
        System.out.println("Sisestasid PIN-koodi 3 korda valesti. Kaart konfiskeeritakse.");
        return false;
    }
    static void saveAllBalances(int balance) {
        try {
            FileWriter fileWriter = new FileWriter("allbalances.txt", true);
            fileWriter.append(balance + "€ " + currentDateTimeToString() + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }

    }
    static void loadAllBalance() {
        // Toimub balance välja lugemine balance.txt failist
        try {
            FileReader fileReader = new FileReader("allbalances.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
    }
}
