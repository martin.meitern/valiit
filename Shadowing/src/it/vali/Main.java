package it.vali;

public class Main {
    static int a = 3;
    public static void main(String[] args) {
        int b = 7;
        System.out.println(b + a);

        a = 0;
        System.out.println(b + a);
        System.out.println(increaseByA(10));

//        Shadowing ehk seespool defineeritud muutuja varjutab väljaspool oleva muutuja
        int a = 6;

        System.out.println(b + a);

//        Nii saab klassi muutuja a väärtust muuta
        Main.a = 8;
//        meetodis defineeritud a väärtuse muutmine
        a = 5;
        System.out.println(increaseByA(10));
        System.out.println(a);
        System.out.println(Main.a);

    }
    static int increaseByA(int b) {
        return b += a;
    }
}
